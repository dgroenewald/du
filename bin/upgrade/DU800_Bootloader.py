#!/usr/bin/env python
#
# DU800_Bootloader.py - Bootload DU800 via raw TCP/IP.
#
# Author: David Groenewald
# Date: 15/08/2018
#

import socket
import sys
import struct
import array
#import numpy as np
import time
import re
import os
from Tkinter import Tk
from tkFileDialog import askopenfilename

ipAddress = '10.66.7.170'

# ---------------------------------------- #
# Variables                                #
# ---------------------------------------- #
BUFFER_SIZE = 4095 # Socket receive buffer size
ACK      =  '041001fb' # ACK response message from DC
NACK     = ['00', '04', 'FE', 'A0'] # NACK response message from DC
CRC_FAIL = ['00', '05', 'FE', '50'] # CRC-8 failed reponse message from DC
val = 0;

# ---------------------------------------- #
# Get IP ADDRESS from user to connect to   #
# ---------------------------------------- #
#while True:
#    ip_address = raw_input("Enter IP Address: ")
#    try:
#        socket.inet_aton(ip_address)
#    except socket.error:
#        print 'Invalid IP Address'
#        continue
#    break

#port = raw_input("Enter PORT: ")
#port = int(port)

# ---------------------------------------- #
# Create a connection                      #
# ---------------------------------------- #
try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print "Socket successfully created \n"
except socket.error as err:
    print "socket creation failed with error %s" %(err)

# connecting to the server
s.settimeout(15);
s.connect((ipAddress,5001))

# ---------------------------------------- #
# Put the DU into Bootloader Mode          #
# ---------------------------------------- #
#-- Get Current FW Version (0x06 0x00 0x01 0x00 0x00 CRC) --#
print("Requesting FW Version") 
FW_VERSION = '\x04\x00\x02\xA5'
s.send(FW_VERSION)
time.sleep(.100)

#-- Wait for response from DC --#
data = s.recv(BUFFER_SIZE)
hex_data = data.encode('hex')
print 'Current Firmware Verion: ', hex_data[4:8]

#-- Send Bootload Command (0x06 0x00 0x01 0x00 0x00 CRC) --#
BOOTLOAD = '\x04\x00\x01\xAC'
s.send(BOOTLOAD)
time.sleep(.1000)

#-- Wait for response from DC --#
data = s.recv(BUFFER_SIZE)
hex_data = data.encode('hex')
print hex_data
# ---------------------------------------- #
# Bootload DU800                           #
# ---------------------------------------- #
if (hex_data == ACK):
    # Send a request for the correct user bin to be flashed.
    s.send('OTA\r\nGetNextFlash\r\n')

    f = None
    response = s.recv(BUFFER_SIZE)
    print response
    if (response == "SendFile\r\n"):
#    if (response == "Bin 1\r\n"):
#        print "Currently in BIN 1"
        print "Please select Encrypted .bin file"
        Tk().withdraw() # we don't want a full GUI, so keep the root window from appearing
        user1bin = askopenfilename() # show an "Open" dialog box and return the path to the selected file
        print 'Flashing \"{}\"...'.format(user1bin)
        f = open(user1bin, "rb")
#   else:
#       print "Currently in BIN 2"
#        print "Please select Encrypted .bin file"
#        Tk().withdraw() # we don't want a full GUI, so keep the root window from appearing
#        user1bin = askopenfilename() # show an "Open" dialog box and return the path to the selected file
#        print 'Flashing \"{}\"...'.format(user1bin)
#        f = open(user1bin, "rb")

    # Read the firmware file.
    contents = f.read()
    f.close()

    # Send through the firmware length
    print 'FirmwareLength: {}\r\n'.format(len(contents))
    s.send('FirmwareLength: {}\r\n'.format(len(contents)))

    # Wait until we get the go-ahead.
    response = s.recv(BUFFER_SIZE)
    if response == "Ready\r\n":
        print "DU800 ready to recieve new firmware file"
        print 'Received response: {}'.format(response)
#        sys.exit(3)
    else:
        print "Invalid response received!"
        print 'Received response: {}'.format(response)

    # Send the firmware.
    print 'Sending {} bytes of firmware'.format(len(contents))
    s.sendall(contents)
    s.settimeout(40.0)
    response = s.recv(BUFFER_SIZE)
    if len(response) > 0:
        print 'Received response: {}'.format(response)

        


    # Close the connection, as we're now done.
    s.close()

    time.sleep(5)
    
    # ---------------------------------------- #
    # Create a connection                      #
    # ---------------------------------------- #
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print "Socket successfully created \n"
    except socket.error as err:
        print "socket creation failed with error %s" %(err)

    # connecting to the server
    s.settimeout(30);
    s.connect((ipAddress,5001))

    # ---------------------------------------- #
    # Put the DU into Bootloader Mode          #
    # ---------------------------------------- #
    #-- Get Current FW Version (0x06 0x00 0x01 0x00 0x00 CRC) --#
    FW_VERSION = '\x04\x00\x02\xA5'
    s.send(FW_VERSION)
    time.sleep(.100)

    #-- Wait for response from DC --#
    data = s.recv(BUFFER_SIZE)
    hex_data = data.encode('hex')
    print 'Current Firmware Verion: ', hex_data[4:8]
    

    s.close()
    sys.exit(0)
#if (val == 2):
    # Close the connection, as we're now done.
#    s.close()
#    sys.exit(0)
    
