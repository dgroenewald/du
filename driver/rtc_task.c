/**
  ******************************************************************************
  * file    led_blink_task.c
  * author  David Groenewald
  * version V1.0.0
  * date    08/01/2018
  * brief   Create task for flash the LED
  ******************************************************************************
*/

#include "tasks.h"
#include "esp_common.h"
#include "pwmConfig.h"
#include "globals.h"
#include "time.h"

/******************************************************************************
* FunctionName : ledInitTask
* Description  : Creates Task which controls the LED brightness and colour
* Parameters   : none
* Returns      : none
*******************************************************************************/
void rtcInitTask(void) {
//	xTaskCreate(rtcWriteTask, (uint8 const *)"i2cWrite", 176, NULL, i2cWriteTaskPriority, &xi2cWriteTaskHandle);
//	xTaskCreate(rtcReadTask,  (uint8 const *)"i2cWrite", 176, NULL, i2cReadTaskPriority,  &xi2cReadTaskHandle);
}

/******************************************************************************
* FunctionName : ledBlinkTask
* Description  : RTOS Task which controls the LED brightness and colour
* Parameters   : none
* Returns      : none
*******************************************************************************/
/*void rtcWriteTask(void* pvParameters) {
	
	for(;;) {
	}

	vTaskDelete(NULL);
}

void rtcReadTask(void* pvParameters) {
	
	for(;;) {
	}

	vTaskDelete(NULL);
}
*/
/* END OF FILE */
