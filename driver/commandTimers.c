/**
  ******************************************************************************
  * file    wifi_ap_config.c
  * author  David Groenewald
  * version V1.0.0
  * date    08/01/2018
  * brief   Configure ESP as an AP and setup DHCP server + TCP for data transmission
  ******************************************************************************
*/

/* Includes ------------------------------------------------------------------*/
#include "commandTimers.h"
#include "esp_common.h"
#include "espconn.h"
#include "tcpConnHandler.h"
#include "globals.h"

/* Handle to TCP connection structure */
struct espconn *conn;

os_timer_t tcpipPacket_Timeout; // Timer to set timeout for TCPIP packets
os_timer_t pdCommand_Timeout;   // Timer to set timeout for PDx8x response

/******************************************************************************
* FunctionName : tcpipPacketTimeout
* Description  : Function is called when a tcpip packet timer expires.
* Parameters   : none
* Returns      : none
*******************************************************************************/
void tcpipPacketTimeout(void) {
	TCPTXLength = 4;
	TCPTXBuffer[0] = TCPTXLength;
	TCPTXBuffer[1] = NACK;
	TCPTXBuffer[2] = 0x01;

	// Calculate CRC and send
	TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
	espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet
									
	/* Reset TCP RX Buffer Read Index */
	TCPReadIndex = 0;
	TCPTXLength = 0;
	discardPacket = 0;
	msgLength = 0;
	ReadIndex = 0;
}

/******************************************************************************
* FunctionName : commandTimeout
* Description  : Function is called when a response timer for PD to reply expires.
* Parameters   : none
* Returns      : none
*******************************************************************************/
void commandTimeout(void) {
/*	TCPTXLength = 4;
	TCPTXBuffer[0] = TCPTXLength;
	TCPTXBuffer[1] = NACK;
	TCPTXBuffer[2] = 0x02;

	// Calculate CRC and send
	TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
	espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet
*/									
	/* Reset TCP RX Buffer Read Index */
//	TCPReadIndex = 0;
//	TCPTXLength = 0;
}