/**
  ******************************************************************************
  * file    spi_flash_task.c
  * author  David Groenewald
  * version V1.0.0
  * date    08/02/2018
  * brief   Create SPI FLASH read and write tasks
  ******************************************************************************
*/

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#include "tasks.h"
#include "esp_common.h"

/******************************************************************************
* FunctionName : spiFlashInitTask
* Description  : Creates Task which reads & writes to the external SPI FLASH
* Parameters   : none
* Returns      : none
*******************************************************************************/
void spiFlashInitTask(void) {
	/* Create Queue for write task */
//	xQueueSpiFlash = xQueueCreate(255, sizeof(uint16_t));
	
	/* Create SPI FLASH write task */
//	xTaskCreate(spiFlashWriteTask, (uint8 const *)"spiFlashWriteTask", 176, NULL, spiFlashWriteTaskPriority, &xspiFlashWriteTaskHandle);

	/* Create SPI FLASH read task */
//	xTaskCreate(spiFlashReadTask, (uint8 const *)"spiFlashReadTask", 176, NULL, spiFlashReadTaskPriority, &xspiFlashReadTaskHandle);
}

/******************************************************************************
* FunctionName : spiFlashWriteTask
* Description  : RTOS Tasks which writes data, sent through queue, to external SPI FLASH 
* Parameters   : none
* Returns      : none
*******************************************************************************/
/* ADC Read Task */
/*void spiFlashWriteTask(void* pvParameters) {
	uint16_t spiWriteData;

	for(;;) {
        if (xQueueReceive(xQueueSpiFlash, (void *)&spiWriteData, (portTickType)portMAX_DELAY)) {
		}
	}

	vTaskDelete(NULL);
}
*/
/******************************************************************************
* FunctionName : spiFlashReadTask
* Description  : RTOS Tasks which reads data from the external SPI FLASH
* Parameters   : none
* Returns      : none
*******************************************************************************/
/*void spiFlashReadTask(void* pvParameters)
{
	for(;;) {
		if(xSemaphoreTake(xspiReadSemaphoreHandle, portMAX_DELAY)) {
		
			//if(sector >= )

		}
	}

	vTaskDelete(NULL);
}
*/
/* END OF FILE */
