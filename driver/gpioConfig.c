/**
  ******************************************************************************
  * file    gpio_config.c
  * author  David Groenewald
  * version V1.0.0
  * date    18/12/2017
  * brief   Configure GPIO pins
  ******************************************************************************
*/

#include "gpioConfig.h"
#include "esp_common.h"

void GPIO_Init(void)
{
	GPIO_ConfigTypeDef io_out_conf;

	/* Configure SPI FLASH Write Protect Pin (GPIO 5) */
	io_out_conf.GPIO_IntrType	= GPIO_PIN_INTR_DISABLE;
	io_out_conf.GPIO_Mode		= GPIO_Mode_Output;
	io_out_conf.GPIO_Pin		= WR_PROT_IO_PIN;
	io_out_conf.GPIO_Pullup		= GPIO_PullUp_EN;
	gpio_config(&io_out_conf);

	/* Configure GPIO 16 as an output used for SPI FLASH CS*/
	gpio16_output_conf();
//	gpio16_input_conf();
}

/* END OF FILE */
