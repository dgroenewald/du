/**
  ******************************************************************************
  * file    led_blink_task.c
  * author  David Groenewald
  * version V1.0.0
  * date    08/01/2018
  * brief   Create task for flash the LED
  ******************************************************************************
*/

#include "tasks.h"
#include "esp_common.h"
#include "pwmConfig.h"
#include "globals.h"
#include "time.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "uart.h"
#include "otaBootload.h"
#include "wifiConfig_Task.h"

/* variables ----------------------------------------------------------------*/
uint16_t dutyGreen = 0, dutyRed = 0;

/******************************************************************************
* FunctionName : ledInitTask
* Description  : Creates Task which controls the LED brightness and colour
* Parameters   : none
* Returns      : none
*******************************************************************************/
void ledInitTask(void) {
	xTaskCreate(ledBlinkTask, (uint8 const *)"ledTask", 512, NULL, ledTaskPriority, &xLEDTaskHandle);
}

/******************************************************************************
* FunctionName : ledBlinkTask
* Description  : RTOS Task which controls the LED brightness and colour
* Parameters   : none
* Returns      : none
*******************************************************************************/
void ledBlinkTask(void* pvParameters) {
	
	int16_t i = 0, j = 0;
	uint8_t direction = 0, flip = 0;
	connStatus = 0;
	uint16_t timer=0;

	for(;;) {
		/* Check if we are not busy bootloading */
		if(du800State == STATE_NORMAL) {
			/* Increment/Decrement PWM duty cycle */
			if (direction)  { i -= 55, j += 55;}
			if (!direction) { i += 55; j -= 55;}

			/* Determine if LED should be brighted or dimmed */
			if (i > 1023) {
				i = 1023;
				direction = 1;
			}
			if (i <= 0) {
				i = 0;
				direction = 0;
			}

			if (j > 1023) {
				j = 511;
				//direction = 1;
			}
			if (j <= 0) {
				j = 0;
				//direction = 0;
			}

			/* If WiFi connected set Green LED else set Red LED */
			if(connStatus == 1) {
				dutyRed = 0;	// Set duty cycle of Red LED to 0
				dutyGreen = i;	// Increment Green LED duty cycle
				setPWM();		// Update PWM duty cycles		
			}
			else if (connStatus == 0) {
				dutyGreen = 0;	// Set duty cycle of Green LED to 0
				dutyRed = i;	// Increment Green LED duty cycle	
				setPWM();		// Update PWM duty cycles
			}
			else {

			}

			if(rescanEnabled) {
				rescanTimer++;
				if(rescanTimer == 2000) {
					//printf("Rescaning\n\r\n\r");
					rescanTimer = 0;
					trackingVariable = 0;
					vTaskResume(xWiFiConfigTaskHandle);
				}
			}
			vTaskDelay(30/portTICK_RATE_MS); // Delay Task 100ms
		}
		/* If we are busy bootloading toggle Red and Green LED */
		if(du800State == STATE_BOOTLOADING) {
			bootloadTimeoutTimer++;

			if (bootloadTimeoutTimer == 59) {
				bootloadTimeoutTimer = 0;
				resetBootloader = 1;
				i = 1024;
				otaBootload(0, 0);
			}

			vTaskDelay(200 / portTICK_RATE_MS); // Delay Task 100ms
		}
	}

	vTaskDelete(NULL);
}

/* END OF FILE */
