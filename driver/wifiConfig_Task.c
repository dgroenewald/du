/**
  ******************************************************************************
  * file    connectionConfig.c
  * author  David Groenewald
  * version V1.0.0
  * date    05/09/2018
  * brief   Checks the AP or STATION mode config saved in Flash
  *         and configures the DU800 accordingly
  ******************************************************************************
*/

#include <stdint.h>
#include <c_types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stdlib_noniso.h"

#include "esp_common.h"
#include "espconn.h"
#include "uart.h"
#include "tasks.h"
#include "globals.h"

#include "spiFlashAPIs.h"
#include "wifiConfig_Task.h"
#include "tcpConnHandler.h"
#include "otaBootload.h"
#include "esp_wifi.h"

/* FOR DEBUG - REMOVE */
#include "pwmConfig.h"
#include "uart.h"

/* Structure holding the TCP connection information ---------*/
LOCAL struct espconn tcp_conn;

/* TCP specific protocol structure --------------------------*/
LOCAL esp_tcp tcp_proto;

/* Local Variables --------------------------------------------------------*/

/* Variables --------------------------------------------------------------*/
uint8_t wifiMode = 0xFF;        // Variable to set WiFi mode to AP or Station Mode
uint8_t ipAddressMode = 0xFF;   // Variable to set the IP Address to static or DHCP
uint8_t flashPrimed = 0x00; // Variable to read value from SPI Flash

// Two dimensional arrays used for reading and writing saved SSIDs and correcsponfing Passwords
char ssidBuffer[MAX_NUM_OF_SSIDS][MAX_SSID_LENGTH];
char passwdBuffer[MAX_NUM_OF_SSIDS][MAX_PASSWD_LENGTH];

uint8_t discReason = 0; // Reason for disconnecting
int8_t trackingVariable = 0; // Scanning through saved Station to see if any available 
uint8_t ssidLength = 0, passwdLength = 0; // Stores length of the current SSID and Password (AP or Station)

uint8_t tempBuf[MAX_PASSWD_LENGTH];	// Temp buffer used to append MAC address to AP's SSID
uint8_t tempBuf_ptr = 0;

struct station_config config;

/******************************************************************************
* FunctionName : ledInitTask
* Description  : Creates Task which controls the LED brightness and colour
* Parameters   : none
* Returns      : none
*******************************************************************************/
void wifiConfigInitTask(void) {
	xTaskCreate(wifiConfigTask, (uint8 const *)"wifiConfigTask", 512, NULL, wifiConfigTaskPriority, &xWiFiConfigTaskHandle);
}

/******************************************************************************
* FunctionName : wifiModeConfig
* Description  : Checks default wifi Configuration stored in SPI FLASH and 
*                sets the wifi configuration (AP/Station mode with IP address etc)   
* Parameters   : none
* Returns      : none
*******************************************************************************/
void wifiModeConfig(void) {

	/* Read data value at PRIME1_ADDRESS on SPI Flash. If 0xFF assume Flash is empty
     * if flash is empty, write default config to flash */
	SPI_FLASH_Read(&flashPrimed, PRIME1_ADDRESS, 1);
	SPI_FLASH_Read(&flashPrimed, PRIME1_ADDRESS, 1);
	
	// Is SPI Flash empty
    if ((flashPrimed != 0xFD)) {
		flashPrimed = 0xFD; /* Write variable to flash Prime Address to indicate Flash is primed */
		
		/* Avoid Flashing the LED (ledBlink_Task)*/
		du800State = STATE_UNKNOWN; // Set DU800 Status to Unknown

		/* Set LED duty cycles to make LED Yellow */
		dutyRed = 100;		// Set duty cycle of Red LED to 0
		dutyGreen = 1024; 	// Set Green LED duty cycle
		setPWM();			// Update PWM duty cycles

		/* Stop the system watchdog timer to avoid reset during SPI Flash write updates */
		system_soft_wdt_stop();

		wifiMode = MODE_AP;//MODE_STATION; 	// Set Default WiFi Mode to Sation 
        ipAddressMode = STATIC_IP; 	// Set IP Addressing mode to Static IP, Not DHCP 
		
		/* Set Default IP Address */
		tcpComms.ipAddress[0] = DEFAULT_IP_ADDR0;
		tcpComms.ipAddress[1] = DEFAULT_IP_ADDR1;
		tcpComms.ipAddress[2] = DEFAULT_IP_ADDR2;
		tcpComms.ipAddress[3] = DEFAULT_IP_ADDR3;

		/* Set Gateway Address */
		tcpComms.gateway[0] = DEFAULT_GW_ADDR0;
		tcpComms.gateway[1] = DEFAULT_GW_ADDR1;
		tcpComms.gateway[2] = DEFAULT_GW_ADDR2;
		tcpComms.gateway[3] = DEFAULT_GW_ADDR3;

		/* Set Subnet Mask */
		tcpComms.subnet[0] = DEFAULT_NETMASK_ADDR0;
		tcpComms.subnet[1] = DEFAULT_NETMASK_ADDR1;
		tcpComms.subnet[2] = DEFAULT_NETMASK_ADDR2;
		tcpComms.subnet[3] = DEFAULT_NETMASK_ADDR3;

		/* Set Default Port */
		tcpComms.portHigh = (uint8_t)((DEFAULT_PORT & 0xFF00) >> 8);
		tcpComms.portLow  = (uint8_t)(DEFAULT_PORT & 0xFF);

		/* Fill buffer with NULLs */
		memset(&ssidBuffer[0][0],   0,   MAX_SSID_LENGTH);
		memset(&passwdBuffer[0][0], 0,   MAX_PASSWD_LENGTH);

		/* Read the AP MAC Address and concatenate the last 
		 * 3 bytes to the default SSID */
		uint8_t macAddress[6];
		char temp[6];
		char defaultSSID[MAX_SSID_LENGTH];

		// Read MAC address	off the device
		wifi_get_macaddr(SOFTAP_IF, macAddress);

		// Print default SSID into a buffer
		snprintf(defaultSSID, sizeof(AP_SSID), "%s", AP_SSID);

		/* Concatenate last 3 bytes of MAC_Address with the AP SSID */
		strcat(defaultSSID, "_");
		sprintf(temp, "%02X", macAddress[3]);
		strcat(defaultSSID, temp);
		strcat(defaultSSID, "_");
		sprintf(temp, "%02X", macAddress[4]);
		strcat(defaultSSID, temp);
		strcat(defaultSSID, "_");
		sprintf(temp, "%02X", macAddress[5]);
		strcat(defaultSSID, temp);

		/* Move AP SSID and Password into holding buffers */
		memmove(&ssidBuffer[0][0],   defaultSSID,   (sizeof(AP_SSID) + 9));
		memmove(&passwdBuffer[0][0], AP_PASSWD,     sizeof(AP_PASSWD));

		// Update Acces Point SSID and Password
		update_flash(AP_SSID_PWD_ADDRESS);

		/* Fill buffer with NULLs */
		memset(&ssidBuffer[0][0],   0,   MAX_SSID_LENGTH);
		memset(&passwdBuffer[0][0], 0,   MAX_PASSWD_LENGTH);

		/* Fill buffer with NULLs */
		memset(&ssidBuffer[1][0],   0,   MAX_SSID_LENGTH);
		memset(&passwdBuffer[1][0], 0,   MAX_PASSWD_LENGTH);

		/* Move Station-1 SSID and Password into holding buffers */
		memmove(&ssidBuffer[0][0],   STATION_SSID1,   sizeof(STATION_SSID1));
		memmove(&passwdBuffer[0][0], STATION_PASSWD1, sizeof(STATION_PASSWD1));

		/* Move Station-2 SSID and Password into holding buffers */
		memmove(&ssidBuffer[1][0],   STATION_SSID2,   sizeof(STATION_SSID2));
		memmove(&passwdBuffer[1][0], STATION_PASSWD2, sizeof(STATION_PASSWD2));

		// Update Station 1&2 SSIDs and Passwords
		update_flash(STATION_SSID1_ADDRESS);
		update_flash(STATION_PWD1_ADDRESS);
		update_flash(STATION_SSID2_ADDRESS);
		update_flash(STATION_PWD2_ADDRESS);

		// Update default WiFi Mode (AP) and IP Address Mode (Static)
		update_flash(WIFI_IP_MODE_SETTINGS);

		// Set default Ethernet Parms - IP Address, Gateway etc.
		update_flash(AP_MODE_ETH_PARMS);

		/* Set Default IP Address for Station to 0 */
	/*	tcpComms.ipAddress[0] = 0;//10;
		tcpComms.ipAddress[1] = 0;//66;
		tcpComms.ipAddress[2] = 0;//7;
		tcpComms.ipAddress[3] = 0;//170;
    */
		/* Set Gateway Address for Station to 0 */
	/*	tcpComms.gateway[0] = 0;//10;
		tcpComms.gateway[1] = 0;//66;
		tcpComms.gateway[2] = 0;//7;
		tcpComms.gateway[3] = 0;//1;
	*/
		/* Set Subnet Mask for Station to 0 */
	/*	tcpComms.subnet[0] = 0;//255;
		tcpComms.subnet[1] = 0;//255;
		tcpComms.subnet[2] = 0;//255;
		tcpComms.subnet[3] = 0;
	*/
		/* Set Default Port for Station to 0 */
	/*	tcpComms.portHigh = 0;//(uint8_t)((DEFAULT_PORT & 0xFF00) >> 8);
		tcpComms.portLow  = 0;//(uint8_t)(DEFAULT_PORT & 0xFF);
	*/
		/* Update Flash with Station Ethernet Parms */
		update_flash(STATION1_MODE_ETH_PARMS);
		update_flash(STATION2_MODE_ETH_PARMS);

		// Update Flash address to indicate Flash has been loaded with defaults
		update_flash(PRIME_ADDRESS);

		/* Restart the system watchdog timer */
		system_soft_wdt_restart();

		du800State = STATE_NORMAL;	// Set DU800 running mode to normal -> Start flashing LED
  }

	/* Read the saved WiFi mode (AP or Station mode) to configure DU800 */
	SPI_FLASH_Read(&wifiMode, WIFI_MODE_ADDRESS, 1);

	LOOP:
	if (wifiMode == MODE_AP) {
        ipAddressMode = STATIC_IP; /* Set IP Addressing mode to Static IP, Not DHCP */

		/* Read SSID for AP from SPI Flash */
		SPI_FLASH_Read(tempBuf,   AP_SSID_ADDRESS,   MAX_SSID_LENGTH);
		// Get length of the SSID stored in FLASH
		while (tempBuf[tempBuf_ptr] != '\0') {
			ssidBuffer[0][tempBuf_ptr] = tempBuf[tempBuf_ptr];
			tempBuf_ptr++;
		}

		// Set Length variable
		ssidLength = tempBuf_ptr;
		tempBuf_ptr = 0;

		/* Read PASSWORD for AP from SPI Flash */
		SPI_FLASH_Read(tempBuf,   AP_PASSWD_ADDRESS,   MAX_PASSWD_LENGTH);
		// Get length of the SSID stored in FLASH
		while (tempBuf[tempBuf_ptr] != '\0') {
			passwdBuffer[0][tempBuf_ptr] = tempBuf[tempBuf_ptr];
			tempBuf_ptr++;
		}

		// Set Length variable
		passwdLength = tempBuf_ptr;
		tempBuf_ptr = 0;

		/* Read Ethernet Parms for connection from SPI Flash */
		SPI_FLASH_Read(tcpComms.ipAddress, AP_IP1_ADDRESS,       4);
		SPI_FLASH_Read(tcpComms.gateway,   AP_GATEWAY1_ADDRESS,  4);
		SPI_FLASH_Read(tcpComms.subnet,    AP_SUBNET1_ADDRESS,   4);
		SPI_FLASH_Read(&tcpComms.portHigh, AP_PORT_HIGH_ADDRESS, 1);
		SPI_FLASH_Read(&tcpComms.portLow,  AP_PORT_LOW_ADDRESS,  1);

		/* Combine high and low nibble of PORT */
		tcpComms.port = ((tcpComms.portHigh << 8) | (tcpComms.portLow));
    }
    else if(wifiMode == MODE_STATION) {
		rescanEnabled = 1;
		ipAddressMode = STATIC_IP; /* Set IP Addressing mode to Static IP, Not DHCP */

		switch (trackingVariable) {
			case 0: {
				/* Fill SSID and Password Buffers with 0s */
				memset(&ssidBuffer[trackingVariable][0],   0, MAX_SSID_LENGTH);
				memset(&passwdBuffer[trackingVariable][0], 0, MAX_PASSWD_LENGTH);

				/* Read SSID for AP from SPI Flash */
				SPI_FLASH_Read(tempBuf,   STATION_SSID1_ADDRESS,   MAX_SSID_LENGTH);
				// Get length of the SSID stored in FLASH
				while (tempBuf[tempBuf_ptr] != '\0') {
					ssidBuffer[trackingVariable][tempBuf_ptr] = tempBuf[tempBuf_ptr];
					tempBuf_ptr++;
				}

				// Set Length variable
				ssidLength = tempBuf_ptr;
				tempBuf_ptr = 0;

				/* Read PASSWORD for AP from SPI Flash */
				SPI_FLASH_Read(tempBuf,   STATION_PWD1_ADDRESS,   MAX_PASSWD_LENGTH);
				// Get length of the SSID stored in FLASH
				while (tempBuf[tempBuf_ptr] != '\0') {
					passwdBuffer[trackingVariable][tempBuf_ptr] = tempBuf[tempBuf_ptr];
					tempBuf_ptr++;
				}

				// Set Length variable
				passwdLength = tempBuf_ptr;
				tempBuf_ptr = 0;

				/* Clear tcpComms' memeber buffers */
				memset(tcpComms.ipAddress,  0, 4);
				memset(tcpComms.gateway,    0, 4);
				memset(tcpComms.subnet,     0, 4);
				memset(&tcpComms.portHigh,  0, 1);
				memset(&tcpComms.portLow,   0, 1);

				/* Read Ethernet Parms for connection from SPI Flash */
				SPI_FLASH_Read(tcpComms.ipAddress, STATION1_IP1_ADDRESS,       4);
				SPI_FLASH_Read(tcpComms.gateway,   STATION1_GATEWAY1_ADDRESS,  4);
				SPI_FLASH_Read(tcpComms.subnet,    STATION1_SUBNET1_ADDRESS,   4);
				SPI_FLASH_Read(&tcpComms.portHigh, STATION1_PORT_HIGH_ADDRESS, 1);
				SPI_FLASH_Read(&tcpComms.portLow,  STATION1_PORT_LOW_ADDRESS,  1);

				/* Combine high and low nibble of PORT */
				tcpComms.port = ((tcpComms.portHigh << 8) | (tcpComms.portLow));

				break;
			}
			case 1: {
				/* Fill SSID and Password Buffers with 0s */
				memset(&ssidBuffer[trackingVariable][0],   0, MAX_SSID_LENGTH);
				memset(&passwdBuffer[trackingVariable][0], 0, MAX_PASSWD_LENGTH);

				/* Read SSID for AP from SPI Flash */
				SPI_FLASH_Read(tempBuf,   STATION_SSID2_ADDRESS,   MAX_SSID_LENGTH);
				// Get length of the SSID stored in FLASH
				while (tempBuf[tempBuf_ptr] != '\0') {
					ssidBuffer[trackingVariable][tempBuf_ptr] = tempBuf[tempBuf_ptr];
					tempBuf_ptr++;
				}

				// Set Length variable
				ssidLength = tempBuf_ptr;
				tempBuf_ptr = 0;

				/* Read PASSWORD for AP from SPI Flash */
				SPI_FLASH_Read(tempBuf,   STATION_PWD2_ADDRESS,   MAX_PASSWD_LENGTH);
				// Get length of the SSID stored in FLASH
				while (tempBuf[tempBuf_ptr] != '\0') {
					passwdBuffer[trackingVariable][tempBuf_ptr] = tempBuf[tempBuf_ptr];
					tempBuf_ptr++;
				}

				// Set Length variable
				passwdLength = tempBuf_ptr;
				tempBuf_ptr = 0;

				/* Clear tcpComms' memeber buffers */
				memset(tcpComms.ipAddress,  0, 4);
				memset(tcpComms.gateway,    0, 4);
				memset(tcpComms.subnet,     0, 4);
				memset(&tcpComms.portHigh,  0, 1);
				memset(&tcpComms.portLow,   0, 1);

				/* Read Ethernet Parms for connection from SPI Flash */
				SPI_FLASH_Read(tcpComms.ipAddress, STATION2_IP1_ADDRESS,       4);
				SPI_FLASH_Read(tcpComms.gateway,   STATION2_GATEWAY1_ADDRESS,  4);
				SPI_FLASH_Read(tcpComms.subnet,    STATION2_SUBNET1_ADDRESS,   4);
				SPI_FLASH_Read(&tcpComms.portHigh, STATION2_PORT_HIGH_ADDRESS, 1);
				SPI_FLASH_Read(&tcpComms.portLow,  STATION2_PORT_LOW_ADDRESS,  1);
				
				/* Combine high and low nibble of PORT */
				tcpComms.port = ((tcpComms.portHigh << 8) | (tcpComms.portLow));

				break;
			}
			case 2: {
				wifiMode = MODE_AP;
				trackingVariable = 0;
				goto LOOP;
				break;
			}
			case 3: {
				break;
			}
			case 4: {
				break;
			}
		}
    }
    else {
    } 
}

/******************************************************************************
* FunctionName : wifi_init
* Description  : sets up WIFI interface
* Parameters   : none
* Returns      : none
*******************************************************************************/
void wifiConfigTask(void* pvParameters) {

	for(;;) {
		/* Get and/or Set configuration from SPI Flash */	
		wifiModeConfig();
		
		if (wifiMode == MODE_STATION) {
			if (wifi_get_opmode_default() != STATION_MODE) {
				wifi_set_opmode(STATION_MODE);	// Set DU800 up as a Station
			}
			
			memset(&config, 0, sizeof(config));  //set value of config from address of &config to width of size to be value '0'

			// Move SSID & Passwd from ssidBuffer to member of struct holding SSID and PassWD
			memmove(config.ssid,     &ssidBuffer[trackingVariable][0],       ssidLength);
			memmove(config.password, &passwdBuffer[trackingVariable][0],     passwdLength);

     		wifi_station_set_auto_connect(0);
   			wifi_station_disconnect();
			
			wifi_station_set_config_current(&config);	// Set config without writing to flash
			wifi_set_event_handler_cb(wifi_event_cb);	// Set event handler call-back
			wifi_station_connect();						// Station connect to AP
			
			struct station_info * station = wifi_softap_get_station_info();
			
				while (station) {
					station = STAILQ_NEXT(station, next);
				}
			
			wifi_softap_free_station_info(); // Free it by calling functions
		}
		else if (wifiMode == MODE_AP) {
			if (wifi_get_opmode_default() != SOFTAP_MODE) {
				wifi_set_opmode(SOFTAP_MODE); // Set DU800 up as soft - Access Point
			}

			/* Initialise the soft-AP */	
			struct softap_config *config = (struct softap_config *) zalloc(sizeof(struct softap_config));
			/* GET the soft-AP configuration */
			wifi_softap_get_config(config);

			memmove(config->ssid,     &ssidBuffer[0][0],       ssidLength);
			memmove(config->password, &passwdBuffer[0][0],     passwdLength);

			config->authmode = AUTH_WPA_WPA2_PSK;
			config->ssid_len = ssidLength; // or its actual SSID length
			config->max_connection = 4;	

			/* SET ESP8366 soft-AP configuration */
			wifi_set_event_handler_cb(wifi_event_cb);	// Set event handler call-back
			wifi_softap_set_config(config);	// Set WiFi config
			system_phy_set_max_tpw(82);		// Set Signal Stength to Max

			free(config);	// Free config struct

			struct station_info * station = wifi_softap_get_station_info();

			while (station) {
				station = STAILQ_NEXT(station, next);
			}

			wifi_softap_free_station_info(); // Free it by calling functions
		}
		else {
			/* DO SOMEHING */
		}

		if (ipAddressMode == STATIC_IP) {
			if (wifiMode == MODE_AP) { 
				struct ip_info info;
				IP4_ADDR(&info.ip, tcpComms.ipAddress[0], tcpComms.ipAddress[1], tcpComms.ipAddress[2], tcpComms.ipAddress[3]); // set IP
				IP4_ADDR(&info.gw, tcpComms.gateway[0], tcpComms.gateway[1], tcpComms.gateway[2], tcpComms.gateway[3]); // set gateway
				IP4_ADDR(&info.netmask, tcpComms.subnet[0], tcpComms.subnet[1], tcpComms.subnet[2], tcpComms.subnet[3]); // set netmask
			
				wifi_softap_dhcps_stop(); // disable soft-AP DHCP server
				wifi_set_ip_info(SOFTAP_IF, &info);

				/* Setup DHCP Server */
				struct dhcps_lease dhcp_lease;
				IP4_ADDR(&dhcp_lease.start_ip, tcpComms.ipAddress[0], tcpComms.ipAddress[1], tcpComms.ipAddress[2], tcpComms.ipAddress[3]); // set IP
				IP4_ADDR(&dhcp_lease.end_ip, tcpComms.ipAddress[0], tcpComms.ipAddress[1], tcpComms.ipAddress[2], tcpComms.ipAddress[3]); // set IP
				wifi_softap_set_dhcps_lease(&dhcp_lease);
				wifi_softap_dhcps_start(); // enable soft-AP DHCP server
			}
			else if (wifiMode == MODE_STATION) { 
				struct ip_info info;
				IP4_ADDR(&info.ip, tcpComms.ipAddress[0], tcpComms.ipAddress[1], tcpComms.ipAddress[2], tcpComms.ipAddress[3]); // set IP
				IP4_ADDR(&info.gw, tcpComms.gateway[0], tcpComms.gateway[1], tcpComms.gateway[2], tcpComms.gateway[3]); // set gateway
				IP4_ADDR(&info.netmask, tcpComms.subnet[0], tcpComms.subnet[1], tcpComms.subnet[2], 254);//tcpComms.subnet[3]); // set netmask
			
				wifi_station_dhcpc_stop(); // disable station DHCP server
				wifi_set_ip_info(STATION_IF, &info);
			}
		}
		else if (ipAddressMode == DHCP) {
			struct dhcps_lease dhcp_lease;
			if (wifiMode == MODE_AP) {
				/* Setup DHCP Server */
				IP4_ADDR(&dhcp_lease.start_ip, tcpComms.ipAddress[0], tcpComms.ipAddress[1], tcpComms.ipAddress[2], tcpComms.ipAddress[3]); // set IP
				IP4_ADDR(&dhcp_lease.end_ip, tcpComms.ipAddress[0], tcpComms.ipAddress[1], tcpComms.ipAddress[2], 105);
				wifi_softap_set_dhcps_lease(&dhcp_lease);
				wifi_softap_dhcps_start(); // enable soft-AP DHCP server
			}
			else if (wifiMode == MODE_STATION) {
				struct ip_info info;
				IP4_ADDR(&info.ip, DEFAULT_IP_ADDR0, DEFAULT_IP_ADDR1, DEFAULT_IP_ADDR2, DEFAULT_IP_ADDR3); // set IP
				IP4_ADDR(&info.gw, DEFAULT_GW_ADDR0, DEFAULT_GW_ADDR1, DEFAULT_GW_ADDR2, DEFAULT_GW_ADDR3); // set gateway
				wifi_softap_set_dhcps_lease(&dhcp_lease);
				wifi_station_dhcpc_stop(); // disable station DHCP server
			}
		}

		/* Set up the TCP server for data transmission */
		tcp_proto.local_port 	= (uint32_t)(tcpComms.port);
		tcp_conn.type 			= ESPCONN_TCP;
		tcp_conn.state 			= ESPCONN_NONE;
		tcp_conn.proto.tcp 		= &tcp_proto;
		espconn_regist_connectcb(&tcp_conn, tcp_connect_cb);
		espconn_regist_disconcb (&tcp_conn, tcp_disconnect_cb);

		espconn_accept(&tcp_conn);

		vTaskSuspend( NULL );
	}
}

/******************************************************************************
* FunctionName : wifi_event_cb
* Description  : WiFi event handler call-back
* Parameters   : event
* Returns      : none
*******************************************************************************/
void ICACHE_FLASH_ATTR wifi_event_cb(System_Event_t *event) {
	/* Get WiFi connection status events */
	switch (event->event_id)
	{
        case EVENT_STAMODE_CONNECTED:

			trackingVariable = 0;
			
			rescanEnabled = 0;
			rescanTimer   = 0;

			/* Connection was made to AP  */
			/* Initialise the serial port */
			uart_init_new();

	        ota_state = CONNECTION_ESTABLISHED;

			/* Set connection status flag - for purpose of setting LED colour */
			connStatus = 1;	
		break;

		case EVENT_STAMODE_DISCONNECTED:
			/* Set connection status flag - for purpose of setting LED colour */
			connStatus = 0;

	        ota_state = NO_CONNECTION;

			// Get reason for disconnection
			discReason = event->event_info.disconnected.reason;

			if (discReason == REASON_BEACON_TIMEOUT) {
				rescanEnabled = 1;
				rescanTimer   = 0;
				trackingVariable = 0;
			}
			
			// If Disconnected due to incorrect SSID or Password - Try next AP from SPI Flash
			if ((discReason == REASON_NO_AP_FOUND) || (discReason == REASON_802_1X_AUTH_FAILED)) {
				wifi_station_disconnect();
				vTaskResume(xWiFiConfigTaskHandle);
				trackingVariable++;
			}

		break;

		case EVENT_STAMODE_GOT_IP: {
		}

		case EVENT_SOFTAPMODE_STACONNECTED: {
			/* Connection was made to AP  */
			/* Initialise the serial port */
			uart_init_new();

			if (rescanEnabled) {
				rescanEnabled = 0;
				pauseRescan = 1;
			}

			/* Set connection status flag - for purpose of setting LED colour */
			connStatus = 1;	
	        ota_state = CONNECTION_ESTABLISHED;

			break;
		}
		case EVENT_SOFTAPMODE_STADISCONNECTED: {
			/* Client disconnected from AP */
			/* Set connection status flag - for purpose of setting LED colour */
			connStatus = 0;
	        ota_state = NO_CONNECTION;

			if (pauseRescan) { 
				rescanTimer   = 0;
				rescanEnabled = 1;
				pauseRescan   = 0;
			}

			//wifi_station_disconnect();
			vTaskResume(xWiFiConfigTaskHandle);

			break;
		}
		default:
		break; 
	}
}

/* END OF FILE */
