#include <string.h>
#include "aes.h"
#include "crypto.h"

#ifdef __cplusplus
extern "C"
{
#endif

//void cryptoTest(void)
//{
//	// WORKSPACE
//	volatile char wrkspc[16];
//	// INPUT
//	volatile char* plainText = "message";
//	volatile char plainBlock[17];
//	plainBlock[16]=0;
//	padString(plainText,plainBlock);
//	// OUTPUT
//	volatile char encryptedBlock[17];
//	volatile char decryptedBlock[17];
//	encryptedBlock[16]=0;
//	decryptedBlock[16]=0;
//	// KEY
//	volatile char key[16];
//	padString("key",key);
//	// INITIALIZATION VECTOR
//	volatile char iv[16];
//	for(int i=0;i<16;i++) iv[i]=0;
//	// CONTEXT
//	volatile struct AES_ctx ctx;
//	AES_init_ctx_iv(&ctx,key,iv);
//	// ENCRYPT
//	for(int i=0;i<16;i++) wrkspc[i]=plainBlock[i];
//	AES_CBC_encrypt_buffer(&ctx,wrkspc,16);
//	for(int i=0;i<16;i++) encryptedBlock[i]=wrkspc[i];
//	// DECRYPT
//	AES_init_ctx_iv(&ctx,key,iv);
//	for(int i=0;i<16;i++) wrkspc[i]=encryptedBlock[i];
//	AES_CBC_decrypt_buffer(&ctx,wrkspc,16);
//	for(int i=0;i<16;i++) decryptedBlock[i]=wrkspc[i];
//	// PRINT RESULT
//	char msg[128];
//
//	debugStringBlocking("CRYPTO TEST - AES CBC 128bit\r\n");
//
//	sprintf(msg,"Input String: %s\r\n",plainText);
//	debugStringBlocking(msg);
//
//	debugStringBlocking("Input Block: ");
//	printBlock(plainBlock);
//	debugStringBlocking("\r\n");
//
//	debugStringBlocking("Encrypted Block: ");
//	printBlock(encryptedBlock);
//	debugStringBlocking("\r\n");
//
//	debugStringBlocking("Decrypted Block: ");
//	printBlock(decryptedBlock);
//	debugStringBlocking("\r\n");
//
//	sprintf(msg,"Decrypted String: %s\r\n",decryptedBlock);
//	debugStringBlocking(msg);
//
//	debugStringBlocking("END OF TEST\r\n");
//}

//void printBlock(char* block)
//{
//	char msg[8];
//	for(int i=0;i<16;i++)
//	{
//		sprintf(msg,"%.2X ",block[i]);
//		debugStringBlocking(msg);
//	}
//}

void pad(char* msg, short msgLen, char* block)
{
	int i; 
	for(i=0;i<msgLen;i++) block[i]=msg[i];
	for(i=msgLen;i<16;i++) block[i]=0;
	
	return;
}
void padString(char* msg, char* block)
{
	pad(msg,strlen(msg),block);
	return;
}

#ifdef __cplusplus
}
#endif
