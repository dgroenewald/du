/*
 * spi_flash.c
 *
 *  Created on: 02 Mar 2016
 *      Author: David Groenewald
 */


/* Includes ------------------------------------------------------------------*/
#include "spiFlashAPIs.h"
#include "gpio.h"
#include "esp_common.h"
//#include "eagle_soc.h"
#include "spi.h"
#include "wifiConfig_Task.h"
#include "globals.h"

/* Private typedef -----------------------------------------------------------*/
#define SPI_FLASH_PAGESIZE  256
#define DUMMY_BYTE				  0xA5 // -- MAYBE BFh or 8Ch


/* Private define ------------------------------------------------------------*/

/* SPI Flash supported commands */
#define CMD_WRITE     0x02  /* Write to Memory instruction */
#define CMD_WRSR      0x01  /* Write Status Register instruction */
#define CMD_WREN      0x06  /* Write enable instruction */
#define CMD_READ      0x03  /* Read from Memory instruction */
#define CMD_RDSR      0x05  /* Read Status Register instruction  */
#define CMD_SE        0xD8  /* Sector Erase instruction */
#define CMD_BE        0xC7  /* Bulk Erase instruction */
#define WIP_FLAG      0x01  /* Write In Progress (WIP) flag */

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Initializes the peripherals used by the SPI FLASH driver.
  * @param  None
  * @retval None
  */
void SPI_Init(void){
  spi_init(HSPI);
   
}

/**
  * @brief  Update Flash Sectors with new values.
  * @param  Address: address of the sector to be updated.
  * @retval None
  */
void update_flash(uint32_t Address)
{
	/* Stop the system watchdog timer to avoid reset during SPI Flash write updates */
	system_soft_wdt_stop();

  /* SPI FLASH Sector 0 */
  if (Address == AP_MODE_ETH_PARMS) {
    SPI_FLASH_SectorErase(AP_MODE_ETH_PARMS);

    SPI_FLASH_Write(tcpComms.ipAddress,  AP_IP1_ADDRESS,       4);
    SPI_FLASH_Write(tcpComms.subnet,     AP_SUBNET1_ADDRESS,   4);
    SPI_FLASH_Write(tcpComms.gateway,    AP_GATEWAY1_ADDRESS,  4);
    SPI_FLASH_Write(&tcpComms.portHigh,  AP_PORT_HIGH_ADDRESS, 1);
    SPI_FLASH_Write(&tcpComms.portLow,   AP_PORT_LOW_ADDRESS,  1);
  }

  /* SPI FLASH Sector 1 */
  else if (Address == STATION1_MODE_ETH_PARMS) {
    SPI_FLASH_SectorErase(STATION1_MODE_ETH_PARMS);

    SPI_FLASH_Write(tcpComms.ipAddress,  STATION1_IP1_ADDRESS,       4);
    SPI_FLASH_Write(tcpComms.subnet,     STATION1_SUBNET1_ADDRESS,   4);
    SPI_FLASH_Write(tcpComms.gateway,    STATION1_GATEWAY1_ADDRESS,  4);
    SPI_FLASH_Write(&tcpComms.portHigh,  STATION1_PORT_HIGH_ADDRESS, 1);
    SPI_FLASH_Write(&tcpComms.portLow,   STATION1_PORT_LOW_ADDRESS,  1);
  }

  /* SPI FLASH Sector 2 */
  else if (Address == STATION2_MODE_ETH_PARMS) {
    SPI_FLASH_SectorErase(STATION2_MODE_ETH_PARMS);

    SPI_FLASH_Write(tcpComms.ipAddress,  STATION2_IP1_ADDRESS,       4);
    SPI_FLASH_Write(tcpComms.subnet,     STATION2_SUBNET1_ADDRESS,   4);
    SPI_FLASH_Write(tcpComms.gateway,    STATION2_GATEWAY1_ADDRESS,  4);
    SPI_FLASH_Write(&tcpComms.portHigh,  STATION2_PORT_HIGH_ADDRESS, 1);
    SPI_FLASH_Write(&tcpComms.portLow,   STATION2_PORT_LOW_ADDRESS,  1);
  }

  /* SPI FLASH Sector 3 */
  else if (Address == WIFI_IP_MODE_SETTINGS) {
    SPI_FLASH_SectorErase(WIFI_IP_MODE_SETTINGS);

    SPI_FLASH_Write(&wifiMode,      WIFI_MODE_ADDRESS,       1);
    SPI_FLASH_Write(&ipAddressMode, IP_ADDRESS_MODE_ADDRESS, 1);
  }

  /* SPI FLASH Sector 4 */
  else if (Address == AP_SSID_PWD_ADDRESS) {
    SPI_FLASH_SectorErase(AP_SSID_PWD_ADDRESS);

    SPI_FLASH_Write(&ssidBuffer[0][0],   AP_SSID_ADDRESS,   MAX_SSID_LENGTH);
    SPI_FLASH_Write(&passwdBuffer[0][0], AP_PASSWD_ADDRESS, MAX_PASSWD_LENGTH);
  }

  /* SPI FLASH Sector 5 */
  else if (Address == STATION_SSID1_ADDRESS) {
    SPI_FLASH_SectorErase(STATION_SSID1_ADDRESS);

    SPI_FLASH_Write(&ssidBuffer[0][0],   STATION_SSID1_ADDRESS, MAX_SSID_LENGTH);
  }

  else if (Address == STATION_PWD1_ADDRESS) {
    SPI_FLASH_SectorErase(STATION_PWD1_ADDRESS);

    SPI_FLASH_Write(&passwdBuffer[0][0], STATION_PWD1_ADDRESS,  MAX_PASSWD_LENGTH);
  }

  else if (Address == STATION_SSID2_ADDRESS) {
    SPI_FLASH_SectorErase(STATION_SSID2_ADDRESS);

    SPI_FLASH_Write(&ssidBuffer[1][0],   STATION_SSID2_ADDRESS, MAX_SSID_LENGTH);
  }

  else if (Address == STATION_PWD2_ADDRESS) {
    SPI_FLASH_SectorErase(STATION_PWD2_ADDRESS);

    SPI_FLASH_Write(&passwdBuffer[1][0], STATION_PWD2_ADDRESS,  MAX_PASSWD_LENGTH);
  }

  /* SPI FLASH Sector 6 */
  /* SPI FLASH Sector 7 */
  /* SPI FLASH Sector 8 */
  /* SPI FLASH Sector 9 */
  /* SPI FLASH Sector 10 */
  /* SPI FLASH Sector 11 */
  /* SPI FLASH Sector 12 */
  /* SPI FLASH Sector 13 */
  /* SPI FLASH Sector 14 */

  /* SPI FLASH Sector 15 */
  else if (Address == PRIME_ADDRESS) {
    SPI_FLASH_SectorErase(PRIME_ADDRESS);

    SPI_FLASH_Write(&flashPrimed, PRIME1_ADDRESS, 1);
  }

  else {
  }

  /* Restart the system watchdog timer */
  system_soft_wdt_restart();
}

/**
  * @brief  Erases the specified FLASH sector.
  * @param  SectorAddr: address of the sector to erase.
  * @retval None
  */
void SPI_FLASH_SectorErase(uint32_t SectorAddr)
{
    /* Send write enable instruction */
		SPI_FLASH_WriteEnable();

		/* Select the FLASH: Chip Select low */
    SPI_FLASH_CS_LOW();

    /* Sector Erase */
    /* Send Sector Erase instruction */
    spi_tx8(HSPI,CMD_SE);
		/* Send SectorAddr high nibble address byte */
		spi_tx8(HSPI, ((SectorAddr & 0xFF0000) >> 16));
		/* Send SectorAddr medium nibble address byte */
		spi_tx8(HSPI, ((SectorAddr & 0xFF00) >> 8));
		/* Send SectorAddr low nibble address byte */
		spi_tx8(HSPI, (SectorAddr & 0xFF));
    /* Deselect the FLASH: Chip Select high */

    while spi_busy(HSPI); //waiting for spi module available
    SPI_FLASH_CS_HIGH();
    
		/* Wait the end of Flash writing */
		SPI_FLASH_WaitForWriteEnd();
}

/**
  * @brief  Erases the entire FLASH.
  * @param  None
  * @retval None
  */
void SPI_FLASH_BulkErase(void)
{
  /* Send write enable instruction */
  SPI_FLASH_WriteEnable();

  /* Bulk Erase */
  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();
  /* Send Bulk Erase instruction  */
  SPI_FLASH_SendByte(CMD_BE);
  /* Deselect the FLASH: Chip Select high */
  SPI_FLASH_CS_HIGH();

  /* Wait the end of Flash writing */
  SPI_FLASH_WaitForWriteEnd();
}

/**
  * @brief  Writes more than one byte to the FLASH with a single WRITE cycle
  *  (Page WRITE sequence).
  * @note   The number of byte can't exceed the FLASH page size.
  * @param  pBuffer: pointer to the buffer  containing the data to be written
  *   to the FLASH.
  * @param  WriteAddr: FLASH's internal address to write to.
  * @param  NumByteToWrite: number of bytes to write to the FLASH, must be equal
  *   or less than "SPI_FLASH_PAGESIZE" value.
  * @retval None
  */
void SPI_FLASH_PageWrite(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite)
{
  /* Enable the write access to the FLASH */
  SPI_FLASH_WriteEnable();

  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();
  
  /* Send "Write to Memory " instruction */
  spi_tx8(HSPI, CMD_WRITE);
  /* Send WriteAddr high nibble address byte to write to */
  spi_tx8(HSPI, ((WriteAddr & 0xFF0000) >> 16));
  /* Send WriteAddr medium nibble address byte to write to */
  spi_tx8(HSPI, ((WriteAddr & 0xFF00) >> 8));
  /* Send WriteAddr low nibble address byte to write to */
  spi_tx8(HSPI, (WriteAddr & 0xFF));

  /* while there is data to be written on the FLASH */
  while (NumByteToWrite--)
  {
    /* Send the current byte */
    spi_tx8(HSPI, (*pBuffer));
    /* Point on the next byte to be written */
    pBuffer++;
  }

  /* Deselect the FLASH: Chip Select high */
  while spi_busy(HSPI); //waiting for spi module available
  SPI_FLASH_CS_HIGH();

  /* Wait the end of Flash writing */
  SPI_FLASH_WaitForWriteEnd();
}

/**
  * @brief  Writes block of data to the FLASH. In this function, the number of
  *   WRITE cycles are reduced, using Page WRITE sequence.
  * @param  pBuffer: pointer to the buffer  containing the data to be written
  *   to the FLASH.
  * @param  WriteAddr: FLASH's internal address to write to.
  * @param  NumByteToWrite: number of bytes to write to the FLASH.
  * @retval None
  */
void SPI_FLASH_BufferWrite(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite)
{
  uint8_t NumOfPage = 0, NumOfSingle = 0, Addr = 0, count = 0, temp = 0;

  Addr        = (WriteAddr % SPI_FLASH_PAGESIZE);
  count       = (SPI_FLASH_PAGESIZE - Addr);
  NumOfPage   = (NumByteToWrite / SPI_FLASH_PAGESIZE);
  NumOfSingle = (NumByteToWrite % SPI_FLASH_PAGESIZE);

  if (Addr == 0) /* WriteAddr is SPI_FLASH_PAGESIZE aligned  */
  {
    if (NumOfPage == 0) /* NumByteToWrite < SPI_FLASH_PAGESIZE */
    {
      SPI_FLASH_PageWrite(pBuffer, WriteAddr, NumByteToWrite);
    }
    else /* NumByteToWrite > SPI_FLASH_PAGESIZE */
    {
      while (NumOfPage--)
      {
        SPI_FLASH_PageWrite(pBuffer, WriteAddr, SPI_FLASH_PAGESIZE);
        WriteAddr +=  SPI_FLASH_PAGESIZE;
        pBuffer += SPI_FLASH_PAGESIZE;
      }

      SPI_FLASH_PageWrite(pBuffer, WriteAddr, NumOfSingle);
    }
  }
  else /* WriteAddr is not SPI_FLASH_PAGESIZE aligned  */
  {
    if (NumOfPage == 0) /* NumByteToWrite < SPI_FLASH_PAGESIZE */
    {
      if (NumOfSingle > count) /* (NumByteToWrite + WriteAddr) > SPI_FLASH_PAGESIZE */
      {
        temp = NumOfSingle - count;

        SPI_FLASH_PageWrite(pBuffer, WriteAddr, count);
        WriteAddr +=  count;
        pBuffer += count;

        SPI_FLASH_PageWrite(pBuffer, WriteAddr, temp);
      }
      else
      {
        SPI_FLASH_PageWrite(pBuffer, WriteAddr, NumByteToWrite);
      }
    }
    else /* NumByteToWrite > SPI_FLASH_PAGESIZE */
    {
      NumByteToWrite -= count;
      NumOfPage =  NumByteToWrite / SPI_FLASH_PAGESIZE;
      NumOfSingle = NumByteToWrite % SPI_FLASH_PAGESIZE;

      SPI_FLASH_PageWrite(pBuffer, WriteAddr, count);
      WriteAddr +=  count;
      pBuffer += count;

      while (NumOfPage--)
      {
        SPI_FLASH_PageWrite(pBuffer, WriteAddr, SPI_FLASH_PAGESIZE);
        WriteAddr +=  SPI_FLASH_PAGESIZE;
        pBuffer += SPI_FLASH_PAGESIZE;
      }

      if (NumOfSingle != 0)
      {
        SPI_FLASH_PageWrite(pBuffer, WriteAddr, NumOfSingle);
      }
    }
  }
}

/**
  * @brief  Reads a block of data from the FLASH.
  * @param  pBuffer: pointer to the buffer that receives the data read from the FLASH.
  * @param  ReadAddr: FLASH's internal address to read from.
  * @param  NumByteToRead: number of bytes to read from the FLASH.
  * @retval None
  */
void SPI_FLASH_BufferRead(uint8_t* pBuffer, uint32_t ReadAddr, uint16_t NumByteToRead)
{
  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();

  /* Send "Read from Memory " instruction */
  spi_tx8(HSPI, (CMD_READ));
  /* Send ReadAddr high nibble address byte to read from */
  spi_tx8(HSPI, ((ReadAddr & 0xFF0000) >> 16));
  /* Send ReadAddr medium nibble address byte to read from */
  spi_tx8(HSPI, ((ReadAddr & 0xFF00) >> 8));
  /* Send ReadAddr low nibble address byte to read from */
  spi_tx8(HSPI, (ReadAddr & 0xFF));
  
  while (NumByteToRead--) /* while there is data to be read */
  {
    *pBuffer = spi_rx8(HSPI);
    pBuffer++;
    /* Read a byte from the FLASH */
//    spi_tx8(HSPI, DUMMY_BYTE);
    /* Point to the next location where the byte read will be saved */
//    pBuffer++;
  }

  /* Deselect the FLASH: Chip Select high */
  while spi_busy(HSPI); //waiting for spi module available
  SPI_FLASH_CS_HIGH();
}

/**
 * @brief 	Writes more than one byte to the FLASH with a single WRITE cycle
  *  		(Page WRITE sequence).
  * @note   The number of byte can't exceed the FLASH page size.
  * @param  pBuffer: pointer to the buffer  containing the data to be written
  *   		to the FLASH.
  * @param  WriteAddr: FLASH's internal address to write to.
  * @param  NumByteToWrite: number of bytes to write to the FLASH, must be equal
  *   		or less than "SPI_FLASH_PAGESIZE" value.
  * @retval None
  */
void SPI_FLASH_Write(uint8_t *pBuffer, uint32_t WriteAddr, uint16_t NumBytesToWrite)
{
//  while(spi_busy(HSPI)); //waiting for spi module available
//  SPI_FLASH_SectorErase(WriteAddr);
  while(spi_busy(HSPI)); //waiting for spi module available
	SPI_FLASH_BufferWrite(pBuffer, WriteAddr, NumBytesToWrite);
  while(spi_busy(HSPI)); //waiting for spi module available
}

/**
 * @brief	Reads data from internal address on FLASH
 * @note	Will only read a SINGLE byte from address
 * @param	ReadAddr: FLASH's internal address to read from.
 * 			NumBytesToRead: number of bytes to read
 */
void SPI_FLASH_Read(uint8_t *pBuffer, uint32_t ReadAddr, uint16_t NumBytesToRead)
{
  /* Stop the system watchdog timer to avoid reset during SPI Flash write updates */
	system_soft_wdt_stop();

  while(spi_busy(HSPI)); //waiting for spi module available
	SPI_FLASH_BufferRead(pBuffer, ReadAddr, NumBytesToRead);
  while(spi_busy(HSPI)); //waiting for spi module available

	/* Restart the system watchdog timer */
	system_soft_wdt_restart();
}

/**
  * @brief  Reads a byte from the SPI Flash.
  * @note   This function must be used only if the Start_Read_Sequence function
  *   has been previously called.
  * @param  None
  * @retval Byte Read from the SPI Flash.
  */
uint8_t SPI_FLASH_ReadByte(void)
{
  return (SPI_FLASH_SendByte(DUMMY_BYTE));
}

/**
  * @brief  Sends a byte through the SPI interface and return the byte received
  *   from the SPI bus.
  * @param  byte: byte to send.
  * @retval The value of the received byte.
  */
uint8_t SPI_FLASH_SendByte(uint8_t byte)
{
  uint32_t read;

  /* Disable write protection on FLASH */
  WRITE_PROTECT_HIGH();

  spi_tx8(HSPI, byte);
  read = spi_rx8(HSPI);
  
//  uart0_tx_buffer((uint8_t *)&read, 1);

 	/* Return the byte read from the SPI bus */
 	return (uint8_t)read;
}

/**
  * @brief  Sends a Half Word through the SPI interface and return the Half Word
  *   received from the SPI bus.
  * @param  HalfWord: Half Word to send.
  * @retval The value of the received Half Word.
  */
uint16_t SPI_FLASH_SendHalfWord(uint16_t HalfWord)
{
  uint32_t temp[2] = {0};
  uint16_t rxData = 0;
  uint32_t recvData[4] = { 0x04 };

  temp[0] = (HalfWord >> 8) & 0x00FF;
  temp[1] = (uint8_t) (HalfWord & 0x00FF);

	/* Split 16bit word into two 8bit bytes */
//	spi_tx_data.halfWord[0] = (HalfWord >> 8) & 0x00FF;
//	spi_tx_data.halfWord[1] = (uint8_t) (HalfWord & 0x00FF);

 
  rxData = (uint16_t)recvData[1] | ((uint16_t)recvData[0] >> 8) ;
  /* Send Half Word through the SPI_FLASH peripheral */
//  SPIMasterSendData(SpiNum_HSPI, &spi_tx_data);  // Transmit data over SPI

	/* Return the byte read from the SPI bus */
//  SPIMasterRecvData(SpiNum_HSPI, &spi_rx_data);  // Recieve data over SPI

	/* Copy 2 8bit bytes into 16bit variable */
//	memcpy(&spi_rx_data.resultHalfWord, spi_rx_data.halfWord, 2);

	return 1;//rxData;//*(uint16_t *) spi_rx_data.data;
}

/**
  * @brief  Enables the write access to the FLASH.
  * @param  None
  * @retval None
  */
void SPI_FLASH_WriteEnable(void)
{
  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();
  
  /* Send "Write Enable" instruction */
  spi_tx8(HSPI, CMD_WREN);

  /* Deselect the FLASH: Chip Select high */
  while(spi_busy(HSPI)); //waiting for spi module available
  SPI_FLASH_CS_HIGH();
}

/**
  * @brief  Polls the status of the Write In Progress (WIP) flag in the FLASH's
  *   status register and loop until write opertaion has completed.
  * @param  None
  * @retval None
  */
void SPI_FLASH_WaitForWriteEnd(void)
{
  uint8_t flashstatus = 0;
  uint8_t count;

  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();

  /* Send "Read Status Register" instruction */
  spi_tx8(HSPI, CMD_RDSR);

  /* Loop as long as the memory is busy with a write cycle */
  do
  {
    /* Send a dummy byte to generate the clock needed by the FLASH
    and put the value of the status register in FLASH_Status variable */
    spi_tx8(HSPI, DUMMY_BYTE);
    flashstatus = spi_rx8(HSPI);
  }
  while ((flashstatus & WIP_FLAG) == 1); /* Write in progress */

  /* Deselect the FLASH: Chip Select high */
  while spi_busy(HSPI); //waiting for spi module available
  SPI_FLASH_CS_HIGH();
}

/**
  * @brief  Reads FLASH identification.
  * @param  None
  * @retval FLASH identification
  */
uint32_t SPI_FLASH_ReadID(void)
{
  uint32_t Temp = 0, Temp0 = 0, Temp1 = 0, Temp2 = 0;

  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();

  /* Send "RDID " instruction */
  SPI_FLASH_SendByte(0x9F);

  /* Read a byte from the FLASH */
  Temp0 = SPI_FLASH_SendByte(DUMMY_BYTE);

  /* Read a byte from the FLASH */
  Temp1 = SPI_FLASH_SendByte(DUMMY_BYTE);

  /* Read a byte from the FLASH */
  Temp2 = SPI_FLASH_SendByte(DUMMY_BYTE);

  /* Deselect the FLASH: Chip Select high */
  SPI_FLASH_CS_HIGH();

  Temp = (Temp0 << 16) | (Temp1 << 8) | Temp2;

  return Temp;
}

/**
  * @brief  Initiates a read data byte (READ) sequence from the Flash.
  *   This is done by driving the /CS line low to select the device, then the READ
  *   instruction is transmitted followed by 3 bytes address. This function exit
  *   and keep the /CS line low, so the Flash still being selected. With this
  *   technique the whole content of the Flash is read with a single READ instruction.
  * @param  ReadAddr: FLASH's internal address to read from.
  * @retval None
  */
void SPI_FLASH_StartReadSequence(uint32_t ReadAddr)
{
  /* Select the FLASH: Chip Select low */
  SPI_FLASH_CS_LOW();

  /* Send "Read from Memory " instruction */
  SPI_FLASH_SendByte(CMD_READ);

  /* Send the 24-bit address of the address to read from ---------------------*/
  /* Send ReadAddr high nibble address byte */
  SPI_FLASH_SendByte((ReadAddr & 0xFF0000) >> 16);
  /* Send ReadAddr medium nibble address byte */
  SPI_FLASH_SendByte((ReadAddr& 0xFF00) >> 8);
  /* Send ReadAddr low nibble address byte */
  SPI_FLASH_SendByte(ReadAddr & 0xFF);
}

/**
  * @}
  */

/**
  * @}
  */

/******************* (C) COPYRIGHT 2009 STMicroelectronics *****END OF FILE****/

