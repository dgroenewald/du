/**
  ******************************************************************************
  * file    adc_task.c
  * author  David Groenewald
  * version V1.0.0
  * date    08/01/2018
  * brief   Create adc read task to read push button
  ******************************************************************************
*/

#include "tasks.h"
#include "esp_common.h"
#include "ESP_system.h"
#include "time.h"
#include "stdio.h"
#include "globals.h"

#include "pwmConfig.h"
#include "spiFlashAPIs.h"

/******************************************************************************
* FunctionName : adcInitTask
* Description  : Creates Task which reads the ADC
* Parameters   : none
* Returns      : none
*******************************************************************************/
void adcInitTask(void) {

	xTaskCreate(adcReadTask, (uint8 const *)"adcTask", 256, NULL, adcTaskPriority, &xADCTaskHandle);
}

/******************************************************************************
* FunctionName : adcReadTask
* Description  : RTOS Tasks which reads the ADC value every 200ms
* Parameters   : none
* Returns      : none
*******************************************************************************/
/* ADC Read Task */
void adcReadTask(void* pvParameters)
{
	uint8_t timer = 0;
	uint8_t deathPacket[1] = {0x00};
	extern uint8_t wifiMode;        // Variable to set WiFi mode to AP or Station Mode
/*	float my_float = 1230.456;

    unsigned char * hexVals;
    hexVals = (unsigned char *)&my_float;
*/	
	/* Dummy variable to transmit when button is pressed */

	for(;;) {
		uint16 adc_read = system_adc_read(); // Read ADC value

		/* If push button is pressed */
		if((adc_read > 550) && (adc_read < 600)) {
			if(timer > 3) {
				du800State = STATE_UNKNOWN;
				timer = 0;
				
				// Update Flash address to indicate Flash has been loaded with defaults
				SPI_FLASH_SectorErase(PRIME_ADDRESS);

				vTaskResume(xWiFiConfigTaskHandle);
			}

			timer++;
		}

		vTaskDelay(100); // Delay task for 1s
	}

	vTaskDelete(NULL);
}

/* END OF FILE */
