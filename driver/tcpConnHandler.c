/**
  ******************************************************************************
  * file    wifi_ap_config.c
  * author  David Groenewald
  * version V1.0.0
  * date    08/01/2018
  * brief   Configure ESP as an AP and setup DHCP server + TCP for data transmission
  ******************************************************************************
*/

#include <stdint.h>
#include <stdio.h>
#include <c_types.h>

#include "esp_common.h"
#include "espconn.h"
#include "tasks.h"
#include "globals.h"

#include "tcpConnHandler.h"
#include "otaBootload.h"
#include "commandTimers.h"

/* Handle to TCP connection info structure ------------------*/
struct espconn *conn;

/* The IP address of the last system to send us any data ----*/
uint32_t last_addr = 0;

uint16_t ReadIndex	= 0;
uint16_t msgLength	= 0;
uint8_t  discardPacket = 0;
uint16_t i;	

/******************************************************************************
* FunctionName : recv_cb
* Description  : Handles the receiving of information, either from TCP or UDP
* Parameters   : none
* Returns      : none
*******************************************************************************/
void ICACHE_FLASH_ATTR recv_cb(void *arg, char *data, uint16_t len) {

	if(data > 0) {
		if(du800State == STATE_NORMAL) {

			/* Copy the from temp Data Buffer to global TCPRXBUFFER */
			for (i=0; i<len; i++) {
				TCPRXBuffer[ReadIndex] = data[i];
				ReadIndex++;
			}

			// Get expected message length
			msgLength = TCPRXBuffer[0];

			// Set a 1sec timeout for response 
            os_timer_disarm(&tcpipPacket_Timeout);
            os_timer_setfn(&tcpipPacket_Timeout, (os_timer_func_t *)tcpipPacketTimeout, NULL);
            os_timer_arm(&tcpipPacket_Timeout, TCPIP_TIMEOUT, 0);

			if (msgLength == ReadIndex) {	/* Compare the payload length to the recieved message length byte */
	            os_timer_disarm(&tcpipPacket_Timeout);
				TCPRXLength = ReadIndex;
				discardPacket = 0;
				
				/* Check CRC of message */
				if (crc_calculateCRC(TCPRXBuffer, TCPRXLength, FORWARD, 0) == CRC_PASS) {
					TCPReadIndex = 0;
					xSemaphoreGive(xtcpSemaphoreHandle);
				} else {
					/* Return a NACK with 0x01 as error identifier */
					TCPTXLength = 4;
					TCPTXBuffer[0] = TCPTXLength;
					TCPTXBuffer[1] = NACK;
					TCPTXBuffer[2] = 0x03;
					TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
					espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send data over TCP connection
				}
				ReadIndex = 0;
			} else if (ReadIndex < msgLength) {
				/* Total number of bytes received is not equal to length expected */
				discardPacket++;

				/* After 3 receives, discard data packet. Must be garbage */
				if (discardPacket == 2)	{
		            os_timer_disarm(&tcpipPacket_Timeout);
	
					/* Clear all flags and variables */
					discardPacket = 0;
					ReadIndex = 0;
					msgLength = 0;

					TCPTXLength = 4;
					TCPTXBuffer[0] = TCPTXLength;
					TCPTXBuffer[1] = NACK;
					TCPTXBuffer[2] = 0x04;
					TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
					espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send data over TCP connection
				}
			}
			else if (ReadIndex > msgLength){
	            os_timer_disarm(&tcpipPacket_Timeout);

				/* Clear all flags and variables */
				discardPacket = 0;
				ReadIndex = 0;
				msgLength = 0;

				TCPTXLength = 4;
				TCPTXBuffer[0] = TCPTXLength;
				TCPTXBuffer[1] = NACK;
				TCPTXBuffer[2] = 0x05;
				TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
				espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send data over TCP connection
			}
		}
		else if (du800State == STATE_BOOTLOADING) {
			bootloadTimeoutTimer = 0;
			otaBootload(data, len);
		}
		else {
			TCPTXLength = 4;
			TCPTXBuffer[0] = TCPTXLength;
			TCPTXBuffer[1] = NACK;
			TCPTXBuffer[2] = 0x06;
			TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
			espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send data over TCP connection
			ReadIndex = 0;
		}
	} else {
		TCPTXLength = 4;
		TCPTXBuffer[0] = TCPTXLength;
		TCPTXBuffer[1] = NACK;
		TCPTXBuffer[2] = 0x07;
		TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
		espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send data over TCP connection
		ReadIndex = 0;
	}
}

/******************************************************************************
* FunctionName : tcp_connect_cb
* Description  : Call-back when an incoming TCP connection has been established.
* Parameters   : none
* Returns      : none
*******************************************************************************/
void ICACHE_FLASH_ATTR tcp_connect_cb(void *arg) {
	/* Handle to connection information structure */
	conn = (struct espconn *)arg;

	/* Configure the TCP receive call-back */
	espconn_regist_recvcb(conn, recv_cb);

	/* Override the TCP connection timeout - set to 2hours */
	espconn_regist_time(conn, 72000, 0x01);
}

/******************************************************************************
* FunctionName : tcp_disconnect_cb
* Description  : Call-back when an TCP connection is closed.
* Parameters   : none
* Returns      : none
*******************************************************************************/
void ICACHE_FLASH_ATTR tcp_disconnect_cb(void *arg) {
	/* Handle to connection information structure */
	conn = (struct espconn *)arg;
	espconn_disconnect (conn);
}

/* END OF FILE */
