/**
  ******************************************************************************
  * file    adc_task.c
  * author  David Groenewald
  * version V1.0.0
  * date    08/01/2018
  * brief   Create adc read task to read push button
  ******************************************************************************
*/

#include "tasks.h"
#include "esp_common.h"
#include "crc.h"
#include "globals.h"
#include "time.h"

#include "uart.h"
#include "otaBootload.h"
#include "wifiConfig_Task.h"
#include "spiFlashAPIs.h"
#include "pwmConfig.h"

#include "commandTimers.h"

#define FW_VERSION 0x0108

//#define debug

/*--------- Local Variables ---------*/
uint8_t cmdType = 0;

struct espconn *conn;

/******************************************************************************
* FunctionName : processTCPIPInitTask
* Description  : Creates Task to handle sending and processing of TCP data packets
* Parameters   : none
* Returns      : none
*******************************************************************************/
void ICACHE_FLASH_ATTR processTCPInitTask(void) {
	xTaskCreate(processTCPTask, (uint8 const *)"ProcessTCPIP_Task", 512, NULL, processTCPIPTaskPriority, NULL);
}

/******************************************************************************
* FunctionName : sendTCPIPInitTask
* Description  : Creates Task handle for processing incoming TCP data packets
* Parameters   : none
* Returns      : none
*******************************************************************************/
void ICACHE_FLASH_ATTR sendTCPInitTask(void) {
	xTaskCreate(sendTCPTask, (uint8 const *)"SendTCPIP_Task", 512, NULL, sendTCPIPTaskPriority, NULL);
}

/******************************************************************************
* FunctionName : sendTCPIPTask
* Description  : Creates Task handle for sending TCP data packets
* Parameters   : none
* Returns      : none
*******************************************************************************/
void ICACHE_FLASH_ATTR sendTCPTask(void* pvParameters) {

	uint16_t ptr = 0;

	for(;;) {
		if(xSemaphoreTake(xtcpSendSemaphoreHandle, portMAX_DELAY)) 
			espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet
	}

	vTaskDelete(NULL);
}


/******************************************************************************
* FunctionName : ReadprocessTCPIPTask
* Description  : RTOS Task to process TCP data packets
* Parameters   : none
* Returns      : none
*******************************************************************************/
/* Process TCP data Task */
void ICACHE_FLASH_ATTR processTCPTask(void* pvParameters) {

	uint8_t j = 0;

	for(;;) {
		if(xSemaphoreTake(xtcpSemaphoreHandle, (portTickType)portMAX_DELAY)) {

			TCPReadIndex += 1; // Move pointer to Message Type byte => Msg Type Byte

				switch(TCPRXBuffer[TCPReadIndex])
				{
					// PDx8x SPECIFIC COMMAND
					case PD_SPECIFIC: {
						/* Send Data Buffer to PDx8x */
						uart0_tx_buffer(TCPRXBuffer, TCPRXLength);

						// Set a 2.5 sec timeout for response 
//                        os_timer_disarm(&pdCommand_Timeout);
//                        os_timer_setfn(&pdCommand_Timeout, (os_timer_func_t *)commandTimeout, NULL);
//                        os_timer_arm(&pdCommand_Timeout, PD_CMD_TIMEOUT, 0);
						
						// Clear TX buffer
						memmove(TCPRXBuffer, " ", TCPReadIndex);

						/* Reset TCP RX Buffer Read Index */
						TCPReadIndex = 0;
						TCPTXLength = 0;
						break;
					}

					// DU800 SPECIFIC COMMAND
					case DU_SPECIFIC: {

						#ifdef debug	
						int i=0;
						for(i = 0; i<TCPRXLength; i++) {
							printf("%02x ", TCPRXBuffer[i]);
						}
						printf("\n\r");	
						#endif

						TCPReadIndex += 1; // Read message cmd Type
						cmdType = TCPRXBuffer[TCPReadIndex];

						/* Process DU800 Specific Command */
						switch(cmdType)
						{
							// Put the DU800 into bootload mode
							case BOOTLOAD_MODE: {
								#ifdef debug	
									printf("Bootload Mode");
										printf("\n\r");
								#endif

								du800State = STATE_BOOTLOADING;	// Put DU800 in bootload mode

								dutyRed = 1024;		// Set duty cycle of Red LED to 0
								dutyGreen = 1024;	// Set duty cycle of Green LED to max
								setPWM();			// Update PWM duty cycles

								aesInit(); // Call AES Encryption/Decryption Initialisation
								init_crc_table();

								TCPTXLength = 4;
								TCPTXBuffer[0] = TCPTXLength;
								TCPTXBuffer[1] = ACK;
								TCPTXBuffer[2] = BOOTLOAD_MODE;

								// Calculate CRC and send
								TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
//								xSemaphoreGive(xtcpSendSemaphoreHandle);	// Send response over TCP
								espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet
								
								/* Reset TCP RX Buffer Read Index */
								TCPReadIndex = 0;
								TCPTXLength = 0;
								break;
							}

							// Get Current Firmware Version	
							case GET_FIRMWARE_VERSION: {
								#ifdef debug	
									printf("Firmware Version");
										printf("\n\r");
								#endif

								uint16_t version = (uint16_t)FW_VERSION;
								TCPTXLength = 5;
								TCPTXBuffer[0] = TCPTXLength;
								TCPTXBuffer[1] = GET_FIRMWARE_VERSION;
								TCPTXBuffer[2] = (uint8_t)(version >> 8);
								TCPTXBuffer[3] = (uint8_t)(version);

								// Calculate CRC and send
								TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
//								xSemaphoreGive(xtcpSendSemaphoreHandle);	// Send response over TCP
								espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet

								/* Reset TCP RX Buffer Read Index */
								TCPReadIndex = 0;
								TCPTXLength = 0;
								break;
							}

							// Set WiFi Mode to AP or Station
							case GET_SET_WIFI_MODE: {
								// Get WiFi Mode
								if (TCPRXLength <= 4) {
									#ifdef debug	
										printf("GET WiFi Mode");
										printf("\n\r");
									#endif

									/* Read default WiFi Mode stored in Flash */
									SPI_FLASH_Read(&wifiMode, WIFI_MODE_ADDRESS, 1);

									TCPTXLength = 4;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = GET_SET_WIFI_MODE;
									TCPTXBuffer[2] = wifiMode;
								}
								// Set WiFi Mode
								else {
									#ifdef debug	
										printf("SET WiFi Mode");
										printf("\n\r");
									#endif

									uint8_t temp;
									wifiMode = TCPRXBuffer[TCPReadIndex + 1];
									ipAddressMode = STATIC_IP;

									update_flash(WIFI_IP_MODE_SETTINGS);

									TCPTXLength = 4;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = ACK;
									TCPTXBuffer[2] = GET_SET_WIFI_MODE;
								}

								// Calculate CRC and send
								TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
//								xSemaphoreGive(xtcpSendSemaphoreHandle);	// Send response over TCP
								espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet

								/* Reset TCP RX Buffer Read Index */
								TCPReadIndex = 0;
								TCPTXLength = 0;
								break;
							}

							// GET/SET AP Mode SSID
							case GET_SET_AP_SSID: {
								// Get AP SSID
								if (TCPRXLength <= 4) {
									#ifdef debug	
										printf("GET AP SSID");
										printf("\n\r");
									#endif

									uint8_t temp[MAX_SSID_LENGTH] = {0};
									uint8_t i = 0;

									SPI_FLASH_Read(temp, AP_SSID_ADDRESS, MAX_SSID_LENGTH);

									while (temp[i] != '\0') {
										//for (i = 0; i < MAX_SSID_LENGTH; i++) {
										TCPTXBuffer[i+2] = temp[i];
										i++;
									}

									TCPTXLength = 3 + i;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = GET_SET_AP_SSID;
								}
								// Set SSID
								else {
									#ifdef debug	
										printf("SET WiFi Mode");
										printf("\n\r");
									#endif

									uint8_t stringLength = 0;
									uint8_t i;

									memset(&ssidBuffer[0][0],   0, MAX_SSID_LENGTH);
									
									/* Get current AP Password before erasing Flash Sector */
									SPI_FLASH_Read(&passwdBuffer[0][0], AP_PASSWD_ADDRESS, MAX_PASSWD_LENGTH);
									
									stringLength = TCPRXBuffer[0] - 4; // Deduct the Length, Type, CMD and CRC byte to get SSID Length

									for (i = 0; i < stringLength; i++) {
										ssidBuffer[0][i] = TCPRXBuffer[3+i];
									}	

									update_flash(AP_SSID_PWD_ADDRESS);

									TCPTXLength = 4;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = ACK;
									TCPTXBuffer[2] = GET_SET_AP_SSID;
								}

								// Calculate CRC and send
								TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
//								xSemaphoreGive(xtcpSendSemaphoreHandle);	// Send response over TCP
								espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet

								/* Reset TCP RX Buffer Read Index */
								TCPReadIndex = 0;
								TCPTXLength = 0;
								break;
							}

							// GET/SET AP Mode Password
							case GET_SET_AP_PASSWD: {
								// Get AP Password
								if (TCPRXLength <= 4) {
									#ifdef debug	
										printf("GET AP Passwd");
										printf("\n\r");
									#endif

									uint8_t temp[MAX_PASSWD_LENGTH];
									uint8_t i = 0;

									SPI_FLASH_Read(temp, AP_PASSWD_ADDRESS, MAX_PASSWD_LENGTH);

									while (temp[i] != '\0') {
										//for (i = 0; i < MAX_SSID_LENGTH; i++) {
										TCPTXBuffer[i+2] = temp[i];
										i++;
									}

									TCPTXLength = 3 + i;//MAX_PASSWD_LENGTH;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = GET_SET_AP_PASSWD;
								}
								// Set AP Password
								else {
									#ifdef debug	
										printf("SET AP Passwd");
										printf("\n\r");
									#endif

									uint8_t stringLength = 0;
									uint8_t i;

									memset(&passwdBuffer[0][0], 0, MAX_PASSWD_LENGTH);

									/* Get current AP SSID before erasing Flash Sector */
									SPI_FLASH_Read(&ssidBuffer[0][0], AP_SSID_ADDRESS, MAX_SSID_LENGTH);

									stringLength = TCPRXBuffer[0] - 4; // Deduct the Length, Type, CMD and CRC byte to get SSID Length

									for (i = 0; i < stringLength; i++) {
										passwdBuffer[0][i] = TCPRXBuffer[3+i];
									}	

									update_flash(AP_SSID_PWD_ADDRESS);

									TCPTXLength = 4;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = ACK;
									TCPTXBuffer[2] = GET_SET_AP_PASSWD;
								}

								// Calculate CRC and send
								TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
//								xSemaphoreGive(xtcpSendSemaphoreHandle);	// Send response over TCP
								espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet

								/* Reset TCP RX Buffer Read Index */
								TCPReadIndex = 0;
								TCPTXLength = 0;
								break;
							}

							// GET/SET IP Address for AP	
							case GET_SET_AP_IP: {
								// GET AP IP Address
								if (TCPRXLength <= 4) {
									#ifdef debug	
										printf("GET AP IP Address");
										printf("\n\r");
									#endif

									uint8_t temp[4];
									uint8_t i;

									/* Read Ethernet Parms for connection from SPI Flash */
									SPI_FLASH_Read(temp, AP_IP1_ADDRESS, 4);

									TCPTXLength = 7;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = GET_SET_AP_IP;
									
									for (i = 0; i < 4; i++) {
										TCPTXBuffer[2+i] = temp[i];
									}
								}
								// SET AP IP Address
								else {
									#ifdef debug	
										printf("SET AP IP Address");
										printf("\n\r");
									#endif

									tcpComms.ipAddress[0] = TCPRXBuffer[++TCPReadIndex];
									tcpComms.ipAddress[1] = TCPRXBuffer[++TCPReadIndex];
									tcpComms.ipAddress[2] = TCPRXBuffer[++TCPReadIndex];
									tcpComms.ipAddress[3] = TCPRXBuffer[++TCPReadIndex];

									SPI_FLASH_Read(tcpComms.gateway,   AP_GATEWAY1_ADDRESS,  4);
									SPI_FLASH_Read(tcpComms.subnet,    AP_SUBNET1_ADDRESS,   4);
									SPI_FLASH_Read(&tcpComms.portHigh, AP_PORT_HIGH_ADDRESS, 1);
									SPI_FLASH_Read(&tcpComms.portLow,  AP_PORT_LOW_ADDRESS,  1);

									update_flash(AP_MODE_ETH_PARMS);

									TCPTXLength = 4;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = ACK;
									TCPTXBuffer[2] = GET_SET_AP_IP;
								}

								// Calculate CRC and send
								TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
//								xSemaphoreGive(xtcpSendSemaphoreHandle);	// Send response over TCP
								espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet

								/* Reset TCP RX Buffer Read Index */
								TCPReadIndex = 0;
								TCPTXLength = 0;
								break;
							}

							// GET/SET GW Address for AP	
							case GET_SET_AP_GW: {
								// GET AP GW Address
								if (TCPRXLength <= 4) {
									#ifdef debug	
										printf("GET AP Gateway");
										printf("\n\r");
									#endif

									uint8_t temp[4];
									uint8_t i;

									/* Read Ethernet Parms for connection from SPI Flash */
									SPI_FLASH_Read(temp, AP_GATEWAY1_ADDRESS, 4);

									TCPTXLength = 7;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = GET_SET_AP_GW;
									
									for (i = 0; i < 4; i++) {
										TCPTXBuffer[2+i] = temp[i];
									}
								}
								// SET AP GW Address
								else {
									#ifdef debug	
										printf("SET AP Gateway");
										printf("\n\r");
									#endif

									tcpComms.gateway[0] = TCPRXBuffer[++TCPReadIndex];
									tcpComms.gateway[1] = TCPRXBuffer[++TCPReadIndex];
									tcpComms.gateway[2] = TCPRXBuffer[++TCPReadIndex];
									tcpComms.gateway[3] = TCPRXBuffer[++TCPReadIndex];

									SPI_FLASH_Read(tcpComms.ipAddress, AP_IP1_ADDRESS,       4);
									SPI_FLASH_Read(tcpComms.subnet,    AP_SUBNET1_ADDRESS,   4);
									SPI_FLASH_Read(&tcpComms.portHigh, AP_PORT_HIGH_ADDRESS, 1);
									SPI_FLASH_Read(&tcpComms.portLow,  AP_PORT_LOW_ADDRESS,  1);

									update_flash(AP_MODE_ETH_PARMS);

									TCPTXLength = 4;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = ACK;
									TCPTXBuffer[2] = GET_SET_AP_GW;
								}

								// Calculate CRC and send
								TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
//								xSemaphoreGive(xtcpSendSemaphoreHandle);	// Send response over TCP
								espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet

								/* Reset TCP RX Buffer Read Index */
								TCPReadIndex = 0;
								TCPTXLength = 0;
								break;
							}

							// GET/SET Subnet Mask for AP	
							case GET_SET_AP_SUBNET: {
								// GET AP Subnet
								if (TCPRXLength <= 4) {
									#ifdef debug	
										printf("GET AP Subnet");
										printf("\n\r");
									#endif

									uint8_t temp[4];
									uint8_t i;

									/* Read Ethernet Parms for connection from SPI Flash */
									SPI_FLASH_Read(temp, AP_SUBNET1_ADDRESS, 4);

									TCPTXLength = 7;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = GET_SET_AP_SUBNET;
									
									for (i = 0; i < 4; i++) {
										TCPTXBuffer[2+i] = temp[i];
									}
								}
								// SET AP Subnet
								else {
									#ifdef debug	
										printf("SET AP Subnet");
										printf("\n\r");
									#endif

									tcpComms.subnet[0] = TCPRXBuffer[++TCPReadIndex];
									tcpComms.subnet[1] = TCPRXBuffer[++TCPReadIndex];
									tcpComms.subnet[2] = TCPRXBuffer[++TCPReadIndex];
									tcpComms.subnet[3] = TCPRXBuffer[++TCPReadIndex];

									SPI_FLASH_Read(tcpComms.ipAddress, AP_IP1_ADDRESS,       4);
									SPI_FLASH_Read(tcpComms.gateway,   AP_GATEWAY1_ADDRESS,  4);
									SPI_FLASH_Read(&tcpComms.portHigh, AP_PORT_HIGH_ADDRESS, 1);
									SPI_FLASH_Read(&tcpComms.portLow,  AP_PORT_LOW_ADDRESS,  1);

									update_flash(AP_MODE_ETH_PARMS);

									TCPTXLength = 4;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = ACK;
									TCPTXBuffer[2] = GET_SET_AP_SUBNET;
								}

								// Calculate CRC and send
								TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
//								xSemaphoreGive(xtcpSendSemaphoreHandle);	// Send response over TCP
								espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet

								/* Reset TCP RX Buffer Read Index */
								TCPReadIndex = 0;
								TCPTXLength = 0;
								break;
							}
							
							// GET/SET Subnet Mask for AP	
							case GET_SET_AP_PORT: {
								// GET AP PORT
								if (TCPRXLength <= 4) {
									#ifdef debug	
										printf("GET AP Port");
										printf("\n\r");
									#endif

									SPI_FLASH_Read(&tcpComms.portHigh, AP_PORT_HIGH_ADDRESS, 1);
									SPI_FLASH_Read(&tcpComms.portLow,  AP_PORT_LOW_ADDRESS,  1);	
									
									TCPTXLength = 5;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = GET_SET_AP_PORT;
									TCPTXBuffer[2] = tcpComms.portHigh;
									TCPTXBuffer[3] = tcpComms.portLow;
								}
								// SET AP PORT
								else {
									#ifdef debug	
										printf("SET AP Port");
										printf("\n\r");
									#endif

									tcpComms.portHigh = TCPRXBuffer[++TCPReadIndex];
									tcpComms.portLow  = TCPRXBuffer[++TCPReadIndex];

									SPI_FLASH_Read(tcpComms.ipAddress, AP_IP1_ADDRESS,       4);
									SPI_FLASH_Read(tcpComms.gateway,   AP_GATEWAY1_ADDRESS,  4);
									SPI_FLASH_Read(tcpComms.subnet,    AP_SUBNET1_ADDRESS,   4);

									update_flash(AP_MODE_ETH_PARMS);

									TCPTXLength = 4;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = ACK;
									TCPTXBuffer[2] = GET_SET_AP_PORT;
								}

								// Calculate CRC and send
								TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
//								xSemaphoreGive(xtcpSendSemaphoreHandle);	// Send response over TCP
								espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet

								/* Reset TCP RX Buffer Read Index */
								TCPReadIndex = 0;
								TCPTXLength = 0;
								break;
							}

							// GET/SET Station 1 SSID
							case GET_SET_STATION_SSID1: {
								// GET SSID
								if (TCPRXLength <= 4) {
									#ifdef debug	
										printf("GET Station 1 SSID");
										printf("\n\r");
									#endif

									uint8_t ssid1[MAX_SSID_LENGTH];
									uint8_t i = 0;

									SPI_FLASH_Read(ssid1, STATION_SSID1_ADDRESS, MAX_SSID_LENGTH);

									while (ssid1[i] != '\0') {
										//for (i = 0; i < MAX_SSID_LENGTH; i++) {
										TCPTXBuffer[i+2] = ssid1[i];
										i++;
									}

									TCPTXLength = 3 + i;//MAX_SSID_LENGTH;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = GET_SET_STATION_SSID1;
								}
								// SET SSID
								else {
									#ifdef debug	
										printf("SET Station 1 SSID");
										printf("\n\r");
									#endif

									uint8_t stringLength = 0;
									uint8_t i;

									stringLength = TCPRXBuffer[0] - 4; // Deduct the Length, Type, CMD and CRC byte to get SSID Length

									memset(&ssidBuffer[0][0], 0, MAX_SSID_LENGTH);
					
									for (i = 0; i < stringLength; i++) {
										ssidBuffer[0][i] = TCPRXBuffer[3+i];
									}

									update_flash(STATION_SSID1_ADDRESS);

									#ifdef debug
										uint8_t temp[MAX_SSID_LENGTH];
										SPI_FLASH_Read(temp, STATION_SSID1_ADDRESS, MAX_SSID_LENGTH);
										printf("Station 1 SSID:%s", temp);
										printf("\n\r");
									#endif

									TCPTXLength = 4;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = ACK;
									TCPTXBuffer[2] = GET_SET_STATION_SSID1;
								}

								// Calculate CRC and send
								TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
//								xSemaphoreGive(xtcpSendSemaphoreHandle);	// Send response over TCP
								espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet

								/* Reset TCP RX Buffer Read Index */
								TCPReadIndex = 0;
								TCPTXLength = 0;

								break;
							}

							// GET/SET Station 1 Password
							case GET_SET_STATION_PASSWD1: {
								// GET Password
								if (TCPRXLength <= 4) {
									#ifdef debug	
										printf("GET Station 1 Password");
										printf("\n\r");
									#endif

									uint8_t temp[MAX_PASSWD_LENGTH];
									uint8_t i = 0;

									SPI_FLASH_Read(temp, STATION_PWD1_ADDRESS, MAX_PASSWD_LENGTH);

									while (temp[i] != '\0') {
										//for (i = 0; i < MAX_SSID_LENGTH; i++) {
										TCPTXBuffer[i+2] = temp[i];
										i++;
									}

									#ifdef debug
										uint8_t temp_passw[MAX_PASSWD_LENGTH];
										SPI_FLASH_Read(temp_passw, STATION_PWD1_ADDRESS, MAX_PASSWD_LENGTH);
										printf("Station 1 Password:%s", temp_passw);
										printf("\n\r");
									#endif

									TCPTXLength = 3 + i;//MAX_PASSWD_LENGTH;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = GET_SET_STATION_PASSWD1;
								}
								// SET Password
								else {
									#ifdef debug	
										printf("SET Station 1 Password");
										printf("\n\r");
									#endif

									uint8_t stringLength = 0;
									uint8_t i;

									stringLength = TCPRXBuffer[0] - 4; // Deduct the Length, Type, CMD and CRC byte to get SSID Length

									memset(&passwdBuffer[0][0], 0, MAX_PASSWD_LENGTH);

									for (i = 0; i < stringLength; i++) {
										passwdBuffer[0][i] = TCPRXBuffer[3+i];
									}	

									update_flash(STATION_PWD1_ADDRESS);

									#ifdef debug
										uint8_t temp[MAX_PASSWD_LENGTH];
										SPI_FLASH_Read(temp, STATION_PWD1_ADDRESS, MAX_PASSWD_LENGTH);
										printf("Station 1 Password:%s", temp);
										printf("\n\r");
									#endif

									TCPTXLength = 4;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = ACK;
									TCPTXBuffer[2] = GET_SET_STATION_PASSWD1;
								}

								// Calculate CRC and send
								TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
//								xSemaphoreGive(xtcpSendSemaphoreHandle);	// Send response over TCP
								espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet

								/* Reset TCP RX Buffer Read Index */
								TCPReadIndex = 0;
								TCPTXLength = 0;
								break;
							}

							// GET/SET SSID for Station 2
							case GET_SET_STATION_SSID2: {
								// GET Station 2 SSID
								if (TCPRXLength <= 4) {
									uint8_t temp[MAX_SSID_LENGTH];
									uint8_t i = 0;

									SPI_FLASH_Read(temp, STATION_SSID2_ADDRESS, MAX_SSID_LENGTH);

									while (temp[i] != '\0') {
										//for (i = 0; i < MAX_SSID_LENGTH; i++) {
										TCPTXBuffer[i+2] = temp[i];
										i++;
									}

									TCPTXLength = 3 + i;//MAX_SSID_LENGTH;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = GET_SET_STATION_SSID2;
								}
								// SET Station 2 SSID
								else {
									uint8_t stringLength = 0;
									uint8_t i;

									stringLength = TCPRXBuffer[0] - 4; // Deduct the Length, Type, CMD and CRC byte to get SSID Length

									memset(&ssidBuffer[1][0], 0, MAX_SSID_LENGTH);

									for (i = 0; i < stringLength; i++) {
										ssidBuffer[1][i] = TCPRXBuffer[3+i];
									}	

									update_flash(STATION_SSID2_ADDRESS);

									TCPTXLength = 4;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = ACK;
									TCPTXBuffer[2] = GET_SET_STATION_SSID2;
								}

								// Calculate CRC and send
								TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
//								xSemaphoreGive(xtcpSendSemaphoreHandle);	// Send response over TCP
								espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet

								/* Reset TCP RX Buffer Read Index */
								TCPReadIndex = 0;
								TCPTXLength = 0;
								break;
							}

							// GET/SET Password for Station 2
							case GET_SET_STATION_PASSWD2: {
								// GET Station 2 Password
								if (TCPRXLength <= 4) {
									uint8_t temp[MAX_PASSWD_LENGTH];
									uint8_t i = 0;

									SPI_FLASH_Read(temp, STATION_PWD2_ADDRESS, MAX_PASSWD_LENGTH);

									while (temp[i] != '\0') {
										//for (i = 0; i < MAX_SSID_LENGTH; i++) {
										TCPTXBuffer[i+2] = temp[i];
										i++;
									}

									TCPTXLength = 3 + i;//MAX_PASSWD_LENGTH;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = GET_SET_STATION_PASSWD2;
								}
								// SET Station 2 Password
								else {
									uint8_t stringLength = 0;
									uint8_t i;

									stringLength = TCPRXBuffer[0] - 4; // Deduct the Length, Type, CMD and CRC byte to get SSID Length

									memset(&passwdBuffer[1][0], 0, MAX_SSID_LENGTH);

									for (i = 0; i < stringLength; i++) {
										passwdBuffer[1][i] = TCPRXBuffer[3+i];
									}	

									update_flash(STATION_PWD2_ADDRESS);

									TCPTXLength = 4;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = ACK;
									TCPTXBuffer[2] = GET_SET_STATION_PASSWD2;
								}

								// Calculate CRC and send
								TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
//								xSemaphoreGive(xtcpSendSemaphoreHandle);	// Send response over TCP
								espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet

								/* Reset TCP RX Buffer Read Index */
								TCPReadIndex = 0;
								TCPTXLength = 0;
								break;
							}

							// GET/SET IP Address for Station 1	
							case GET_SET_STATION1_IP: {
								// GET Station 1 IP Address
								if (TCPRXLength <= 4) {
									uint8_t temp[4];
									uint8_t i;

									/* Read Ethernet Parms for connection from SPI Flash */
									SPI_FLASH_Read(temp, STATION1_IP1_ADDRESS, 4);

									TCPTXLength = 7;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = GET_SET_STATION1_IP;
									
									for (i = 0; i < 4; i++) {
										TCPTXBuffer[2+i] = temp[i];
									}
								}
								// SET Station 1 IP Address
								else {
									tcpComms.ipAddress[0] = TCPRXBuffer[++TCPReadIndex];
									tcpComms.ipAddress[1] = TCPRXBuffer[++TCPReadIndex];
									tcpComms.ipAddress[2] = TCPRXBuffer[++TCPReadIndex];
									tcpComms.ipAddress[3] = TCPRXBuffer[++TCPReadIndex];

									SPI_FLASH_Read(tcpComms.gateway,   STATION1_GATEWAY1_ADDRESS,  4);
									SPI_FLASH_Read(tcpComms.subnet,    STATION1_SUBNET1_ADDRESS,   4);
									SPI_FLASH_Read(&tcpComms.portHigh, STATION1_PORT_HIGH_ADDRESS, 1);
									SPI_FLASH_Read(&tcpComms.portLow,  STATION1_PORT_LOW_ADDRESS,  1);

									update_flash(STATION1_MODE_ETH_PARMS);

									TCPTXLength = 4;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = ACK;
									TCPTXBuffer[2] = GET_SET_STATION1_IP;
								}

								// Calculate CRC and send
								TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
//								xSemaphoreGive(xtcpSendSemaphoreHandle);	// Send response over TCP
								espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet

								/* Reset TCP RX Buffer Read Index */
								TCPReadIndex = 0;
								TCPTXLength = 0;
								break;
							}

							// GET/SET Gateway Address for Station 1 	
							case GET_SET_STATION1_GW: {
								// GET Gateway Address
								if (TCPRXLength <= 4) {
									uint8_t temp[4];
									uint8_t i;

									/* Read Ethernet Parms for connection from SPI Flash */
									SPI_FLASH_Read(temp, STATION1_GATEWAY1_ADDRESS, 4);

									TCPTXLength = 7;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = GET_SET_STATION1_GW;
									
									for (i = 0; i < 4; i++) {
										TCPTXBuffer[2+i] = temp[i];
									}
								}
								// SET Gateway Address
								else {
									tcpComms.gateway[0] = TCPRXBuffer[++TCPReadIndex];
									tcpComms.gateway[1] = TCPRXBuffer[++TCPReadIndex];
									tcpComms.gateway[2] = TCPRXBuffer[++TCPReadIndex];
									tcpComms.gateway[3] = TCPRXBuffer[++TCPReadIndex];

									SPI_FLASH_Read(tcpComms.ipAddress, STATION1_IP1_ADDRESS,       4);
									SPI_FLASH_Read(tcpComms.subnet,    STATION1_SUBNET1_ADDRESS,   4);
									SPI_FLASH_Read(&tcpComms.portHigh, STATION1_PORT_HIGH_ADDRESS, 1);
									SPI_FLASH_Read(&tcpComms.portLow,  STATION1_PORT_LOW_ADDRESS,  1);

									update_flash(STATION1_MODE_ETH_PARMS);

									TCPTXLength = 4;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = ACK;
									TCPTXBuffer[2] = GET_SET_STATION1_GW;
								}

								// Calculate CRC and send
								TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
//								xSemaphoreGive(xtcpSendSemaphoreHandle);	// Send response over TCP
								espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet

								/* Reset TCP RX Buffer Read Index */
								TCPReadIndex = 0;
								TCPTXLength = 0;
								break;
							}

							// GET/SET Subnet Mask for Station 1	
							case GET_SET_STATION1_SUBNET: {
								// GET SUBNET
								if (TCPRXLength <= 4) {
									uint8_t temp[4];
									uint8_t i;

									/* Read Ethernet Parms for connection from SPI Flash */
									SPI_FLASH_Read(temp, STATION1_SUBNET1_ADDRESS, 4);

									TCPTXLength = 7;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = GET_SET_STATION1_SUBNET;
									
									for (i = 0; i < 4; i++) {
										TCPTXBuffer[2+i] = temp[i];
									}
								}
								// SET SUBNET
								else {
									tcpComms.subnet[0] = TCPRXBuffer[++TCPReadIndex];
									tcpComms.subnet[1] = TCPRXBuffer[++TCPReadIndex];
									tcpComms.subnet[2] = TCPRXBuffer[++TCPReadIndex];
									tcpComms.subnet[3] = TCPRXBuffer[++TCPReadIndex];

									SPI_FLASH_Read(tcpComms.ipAddress, STATION1_IP1_ADDRESS,       4);
									SPI_FLASH_Read(tcpComms.gateway,   STATION1_GATEWAY1_ADDRESS,  4);
									SPI_FLASH_Read(&tcpComms.portHigh, STATION1_PORT_HIGH_ADDRESS, 1);
									SPI_FLASH_Read(&tcpComms.portLow,  STATION1_PORT_LOW_ADDRESS,  1);

									update_flash(STATION1_MODE_ETH_PARMS);

									TCPTXLength = 4;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = ACK;
									TCPTXBuffer[2] = GET_SET_STATION1_SUBNET;
								}

								// Calculate CRC and send
								TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
//								xSemaphoreGive(xtcpSendSemaphoreHandle);	// Send response over TCP
								espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet

								/* Reset TCP RX Buffer Read Index */
								TCPReadIndex = 0;
								TCPTXLength = 0;
								break;
							}

							// GET/SET IP Address for Station 2	
							case GET_SET_STATION2_IP: {
								// GET IP Address
								if (TCPRXLength <= 4) {
									uint8_t temp[4];
									uint8_t i;

									/* Read Ethernet Parms for connection from SPI Flash */
									SPI_FLASH_Read(temp, STATION2_IP1_ADDRESS, 4);

									TCPTXLength = 7;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = GET_SET_STATION2_IP;
									
									for (i = 0; i < 4; i++) {
										TCPTXBuffer[2+i] = temp[i];
									}
								}
								// SET IP Address
								else {
									tcpComms.ipAddress[0] = TCPRXBuffer[++TCPReadIndex];
									tcpComms.ipAddress[1] = TCPRXBuffer[++TCPReadIndex];
									tcpComms.ipAddress[2] = TCPRXBuffer[++TCPReadIndex];
									tcpComms.ipAddress[3] = TCPRXBuffer[++TCPReadIndex];

									SPI_FLASH_Read(tcpComms.subnet,    STATION2_SUBNET1_ADDRESS,   4);
									SPI_FLASH_Read(tcpComms.gateway,   STATION2_GATEWAY1_ADDRESS,  4);
									SPI_FLASH_Read(&tcpComms.portHigh, STATION2_PORT_HIGH_ADDRESS, 1);
									SPI_FLASH_Read(&tcpComms.portLow,  STATION2_PORT_LOW_ADDRESS,  1);

									update_flash(STATION2_MODE_ETH_PARMS);

									TCPTXLength = 4;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = ACK;
									TCPTXBuffer[2] = GET_SET_STATION2_IP;
								}

								// Calculate CRC and send
								TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
//								xSemaphoreGive(xtcpSendSemaphoreHandle);	// Send response over TCP
								espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet

								/* Reset TCP RX Buffer Read Index */
								TCPReadIndex = 0;
								TCPTXLength = 0;
								break;
							}

							// GET/SET Gateway Address for Station 2 	
							case GET_SET_STATION2_GW: {
								// GET Gateway
								if (TCPRXLength <= 4) {
									uint8_t temp[4];
									uint8_t i;
									/* Read Ethernet Parms for connection from SPI Flash */
									SPI_FLASH_Read(temp, STATION2_GATEWAY1_ADDRESS, 4);

									TCPTXLength = 7;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = GET_SET_STATION2_GW;
									
									for (i = 0; i < 4; i++) {
										TCPTXBuffer[2+i] = temp[i];
									}
								}
								// SET Gateway
								else {
									tcpComms.gateway[0] = TCPRXBuffer[++TCPReadIndex];
									tcpComms.gateway[1] = TCPRXBuffer[++TCPReadIndex];
									tcpComms.gateway[2] = TCPRXBuffer[++TCPReadIndex];
									tcpComms.gateway[3] = TCPRXBuffer[++TCPReadIndex];

									SPI_FLASH_Read(tcpComms.subnet,    STATION2_SUBNET1_ADDRESS,   4);
									SPI_FLASH_Read(tcpComms.ipAddress, STATION2_IP1_ADDRESS,       4);
									SPI_FLASH_Read(&tcpComms.portHigh, STATION2_PORT_HIGH_ADDRESS, 1);
									SPI_FLASH_Read(&tcpComms.portLow,  STATION2_PORT_LOW_ADDRESS,  1);

									update_flash(STATION2_MODE_ETH_PARMS);

									TCPTXLength = 4;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = ACK;
									TCPTXBuffer[2] = GET_SET_STATION2_GW;
								}

								// Calculate CRC and send
								TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
//								xSemaphoreGive(xtcpSendSemaphoreHandle);	// Send response over TCP
								espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet

								/* Reset TCP RX Buffer Read Index */
								TCPReadIndex = 0;
								TCPTXLength = 0;
								break;
							}

							// GET/SET Subnet Mask for Station 2	
							case GET_SET_STATION2_SUBNET: {
								// GET Subnet
								if (TCPRXLength <= 4) {
									uint8_t temp[4];
									uint8_t i;

									/* Read Ethernet Parms for connection from SPI Flash */
									SPI_FLASH_Read(temp, STATION2_SUBNET1_ADDRESS, 4);

									TCPTXLength = 7;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = GET_SET_STATION2_SUBNET;
									
									for (i = 0; i < 4; i++) {
										TCPTXBuffer[2+i] = temp[i];
									}
								}
								// SET Subnet
								else {
									tcpComms.subnet[0] = TCPRXBuffer[++TCPReadIndex];
									tcpComms.subnet[1] = TCPRXBuffer[++TCPReadIndex];
									tcpComms.subnet[2] = TCPRXBuffer[++TCPReadIndex];
									tcpComms.subnet[3] = TCPRXBuffer[++TCPReadIndex];

									SPI_FLASH_Read(tcpComms.gateway,   STATION2_GATEWAY1_ADDRESS,  4);
									SPI_FLASH_Read(tcpComms.ipAddress, STATION2_IP1_ADDRESS,       4);
									SPI_FLASH_Read(&tcpComms.portHigh, STATION2_PORT_HIGH_ADDRESS, 1);
									SPI_FLASH_Read(&tcpComms.portLow,  STATION2_PORT_LOW_ADDRESS,  1);

									update_flash(STATION2_MODE_ETH_PARMS);

									TCPTXLength = 4;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = ACK;
									TCPTXBuffer[2] = GET_SET_STATION2_SUBNET;
								}

								// Calculate CRC and send
								TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
//								xSemaphoreGive(xtcpSendSemaphoreHandle);	// Send response over TCP
								espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet

								/* Reset TCP RX Buffer Read Index */
								TCPReadIndex = 0;
								TCPTXLength = 0;
								break;
							}

							// GET/SET Port for Station 1	
							case GET_SET_STATION1_PORT: {
								// GET port
								if (TCPRXLength <= 4) {
									SPI_FLASH_Read(&tcpComms.portHigh, STATION1_PORT_HIGH_ADDRESS, 1);
									SPI_FLASH_Read(&tcpComms.portLow,  STATION1_PORT_LOW_ADDRESS,  1);	
									
									TCPTXLength = 5;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = GET_SET_STATION1_PORT;
									TCPTXBuffer[2] = tcpComms.portHigh;
									TCPTXBuffer[3] = tcpComms.portLow;
								}
								// SET Port
								else {
									tcpComms.portHigh = TCPRXBuffer[++TCPReadIndex];
									tcpComms.portLow  = TCPRXBuffer[++TCPReadIndex];

									SPI_FLASH_Read(tcpComms.gateway,   STATION1_GATEWAY1_ADDRESS,  4);
									SPI_FLASH_Read(tcpComms.ipAddress, STATION1_IP1_ADDRESS,       4);
									SPI_FLASH_Read(tcpComms.subnet,    STATION1_SUBNET1_ADDRESS,   4);

									update_flash(STATION1_MODE_ETH_PARMS);

									TCPTXLength = 4;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = ACK;
									TCPTXBuffer[2] = GET_SET_STATION1_PORT;
								}

								// Calculate CRC and send
								TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
//								xSemaphoreGive(xtcpSendSemaphoreHandle);	// Send response over TCP
								espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet

								/* Reset TCP RX Buffer Read Index */
								TCPReadIndex = 0;
								TCPTXLength = 0;
								break;
							}

							// GET/SET Port for Station 2	
							case GET_SET_STATION2_PORT: {
								// GET Port
								if (TCPRXLength <= 4) {
									SPI_FLASH_Read(&tcpComms.portHigh, STATION2_PORT_HIGH_ADDRESS, 1);
									SPI_FLASH_Read(&tcpComms.portLow,  STATION2_PORT_LOW_ADDRESS,  1);	
									
									TCPTXLength = 5;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = GET_SET_STATION2_PORT;
									TCPTXBuffer[2] = tcpComms.portHigh;
									TCPTXBuffer[3] = tcpComms.portLow;
								}
								// SET Port
								else {
									tcpComms.portHigh = TCPRXBuffer[++TCPReadIndex];
									tcpComms.portLow  = TCPRXBuffer[++TCPReadIndex];

									SPI_FLASH_Read(tcpComms.gateway,   STATION2_GATEWAY1_ADDRESS,  4);
									SPI_FLASH_Read(tcpComms.ipAddress, STATION2_IP1_ADDRESS,       4);
									SPI_FLASH_Read(tcpComms.subnet,    STATION2_SUBNET1_ADDRESS,   4);

									update_flash(STATION2_MODE_ETH_PARMS);

									TCPTXLength = 4;
									TCPTXBuffer[0] = TCPTXLength;
									TCPTXBuffer[1] = ACK;
									TCPTXBuffer[2] = GET_SET_STATION2_PORT;
								}

								// Calculate CRC and send
								TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
//								xSemaphoreGive(xtcpSendSemaphoreHandle);	// Send response over TCP
								espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet

								/* Reset TCP RX Buffer Read Index */
								TCPReadIndex = 0;
								TCPTXLength = 0;
								break;
							}

							// Get STATION's MAC Address
							case GET_SOFTAP_MAC_ADDRESS:{
								uint8_t mac[6];
								uint8_t i;
								
								wifi_get_macaddr(SOFTAP_IF, mac);
								
								TCPTXLength = 9;
								TCPTXBuffer[0] = TCPTXLength;
								TCPTXBuffer[1] = GET_SOFTAP_MAC_ADDRESS;

								for(i = 0; i < 6; i++) {
									TCPTXBuffer[i+2] = mac[i];
								}
								
								// Calculate CRC and send
								TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
//								xSemaphoreGive(xtcpSendSemaphoreHandle);	// Send response over TCP
								espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet

								/* Reset TCP RX Buffer Read Index */
								TCPReadIndex = 0;
								TCPTXLength = 0;
								break;
							}

							// Get STATION's MAC Address
							case GET_STATION_MAC_ADDRESS: {
								uint8_t mac[6];
								uint8_t i;
								
								wifi_get_macaddr(STATION_IF, mac);
								
								TCPTXLength = 9;
								TCPTXBuffer[0] = TCPTXLength;
								TCPTXBuffer[1] = GET_STATION_MAC_ADDRESS;

								for(i = 0; i < 6; i++) {
									TCPTXBuffer[i+2] = mac[i];
								}
								
								// Calculate CRC and send
								TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
//								xSemaphoreGive(xtcpSendSemaphoreHandle);	// Send response over TCP
								espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet

								/* Reset TCP RX Buffer Read Index */
								TCPReadIndex = 0;
								TCPTXLength = 0;
								break;
							}

							// Reboot DU800	
							case SYSTEM_REBOOT: {
								TCPTXLength = 4;
								TCPTXBuffer[0] = TCPTXLength;
								TCPTXBuffer[1] = ACK;
								TCPTXBuffer[2] = SYSTEM_REBOOT;
								TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
//								xSemaphoreGive(xtcpSendSemaphoreHandle);	// Send response over TCP
								espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet

								uint16 timer = 0;
								for (timer = 0; timer < 60000; timer++){}

								system_restart();
								break;
							}

							// Set Date and Time for RTC	
							case SET_DATE_TIME: {
								break;
							}

							default:
								TCPTXLength = 4;
								TCPTXBuffer[0] = TCPTXLength;
								TCPTXBuffer[1] = ACK;
								TCPTXBuffer[2] = cmdType;
								TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
//								xSemaphoreGive(xtcpSendSemaphoreHandle);	// Send response over TCP
								espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send TCP Data packet
						}
					}
					default:
                        TCPTXLength = 4;
                        TCPTXBuffer[0] = TCPTXLength;
                        TCPTXBuffer[1] = NACK;
                        TCPTXBuffer[2] = 0x00;
                        TCPTXBuffer[TCPTXLength-1] = crc_calculateCRC(TCPTXBuffer, TCPTXLength-1, FORWARD, 0);
                        espconn_send(conn, TCPTXBuffer, TCPTXLength); // Send data over TCP connection

						break;
				}
			
		}
	}

	vTaskDelete(NULL);
}

/* END OF FILE */
