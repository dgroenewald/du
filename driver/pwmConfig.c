/**
  ******************************************************************************
  * file    pwm_config.c
  * author  David Groenewald
  * version V1.0.0
  * date    18/12/2017
  * brief   Configure PWM peripheral to drive LEDs
  ******************************************************************************
*/

#include "pwmConfig.h"

/* variables ----------------------------------------------------------------*/
u32 duty[2] = {0, 0};

/* PWM peripheral initialisation */
void PWM_Init(void)
{
	uint32 io_info[][3] = {
		{ PWM_1_OUT_IO_MUX, PWM_1_OUT_IO_FUNC, PWM_1_OUT_IO_NUM },
		{ PWM_2_OUT_IO_MUX, PWM_2_OUT_IO_FUNC, PWM_2_OUT_IO_NUM }
	};

	pwm_init(10000, duty, 2, io_info); // Switch at 1kHz

}

/* Fucntion to set duty cycle for a specific channel */
void setPWM()
{
	pwm_set_duty(dutyRed, 0);	// Set Red LED duty cycle
	pwm_set_duty(dutyGreen, 1); // Set Green LED duty cycle
	pwm_start();   				// Call this every time you change duty/period
}

/* END OF FILE */
