#ifndef CRYPTO_H
#define CRYPTO_H
#ifdef __cplusplus
extern "C"
{
#endif

void cryptoTest(void);
void printBlock(char* block);
void pad(char* msg, short msgLen, char* block);
void padString(char* msg, char* block);

#ifdef __cplusplus
}
#endif
#endif
