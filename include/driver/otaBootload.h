/**
  ******************************************************************************
  * @file    pwm_config.h
  * @author  David Groenewald
  * @version V1.0.0
  * @date    18/12/2017
  * @brief   Header file for PWM pin configuration.
  ******************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __OTA_BOOTLOAD_H__
#define __OTA_BOOTLOAD_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/

/* Defines -------------------------------------------------------------------*/

/* Variables -----------------------------------------------------------------*/
extern uint32_t bootloadTimeoutTimer;  // A Timer to release DU from bootload mode if no data is recieved
extern uint8_t resetBootloader;

/* Encryption variables */
extern char key[16];
extern char iv[16];

typedef enum {
    NOT_STARTED,
    CONNECTION_ESTABLISHED,
    NO_CONNECTION,
    RECEIVING_HEADER,
    RECEIVING_FIRMWARE,
    REBOOTING,
    ERROR
} OTA_STATE_T;

extern OTA_STATE_T ota_state; // extern for the instance

/* Macros --------------------------------------------------------------------*/

/* Function Prototypes -------------------------------------------------------*/
void aesInit();
void otaBootload(char *otaData, uint16_t len);
#endif //_PWM_CONFIG_H_

/* END OF FILE */
