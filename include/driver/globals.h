/**
  ******************************************************************************
  * @file    globals.h
  * @author  David Groenewald
  * @version V1.0.0
  * @date    10/05/2016
  * @brief   Header file for global variables.
  ******************************************************************************
  */ 
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _GLOBALS_H
#define _GLOBALS_H

#ifdef __cplusplus
 extern "C" {
#endif 

/* Includes ------------------------------------------------------------------*/
#include "esp_common.h"

 /* Defines -------------------------------------------------------------------*/
 #define ERR_OK			0
 #define ACK				0x10
 #define NACK				0xFE
 #define CLEAR      0
 #define SET        1
 
// DU800 Status Flags
 #define STATE_NORMAL       1
 #define STATE_BOOTLOADING  2
 #define STATE_UNKNOWN      4

// TCPIP (Ethernet) Comms Flags
 #define TCPIP_SEND				1
 #define TCPIP_RECEIVE		2

// DU/PD Command Types
 #define DU_SPECIFIC  0
 #define PD_SPECIFIC  1

// DU Specific commands
 #define BOOTLOAD_MODE            1
 #define GET_FIRMWARE_VERSION     2
 #define GET_SET_WIFI_MODE        3
 
 #define GET_SET_AP_SSID          5
 #define GET_SET_AP_PASSWD        6
  
 #define GET_SET_AP_IP            7
 #define GET_SET_AP_GW            8
 #define GET_SET_AP_SUBNET        9
 #define GET_SET_AP_PORT          10

 #define GET_SET_STATION_SSID1     11
 #define GET_SET_STATION_PASSWD1   12
 #define GET_SET_STATION_SSID2     13
 #define GET_SET_STATION_PASSWD2   14
 
 #define GET_SET_STATION1_IP       15
 #define GET_SET_STATION1_GW       16
 #define GET_SET_STATION1_SUBNET   17
 #define GET_SET_STATION1_PORT     18

 #define GET_SET_STATION2_IP       19
 #define GET_SET_STATION2_GW       20
 #define GET_SET_STATION2_SUBNET   21
 #define GET_SET_STATION2_PORT     22

 #define GET_SOFTAP_MAC_ADDRESS    34
 #define GET_STATION_MAC_ADDRESS   35

 #define SYSTEM_REBOOT             100

 #define SET_DATE_TIME             255

/* Transmit/Receive Buffer Lengths ------------------------------------------*/
 #define TCP_TX_BUF_LEN		1024
 #define TCP_RX_BUF_LEN	  1024
 #define UART_TX_BUF_LEN  1024
 #define UART_RX_BUF_LEN  1024

 /* Typedef -------------------------------------------------------------------*/
   typedef struct {
    uint8_t ipAddress[4];
    uint8_t gateway[4];
    uint8_t subnet[4];
    uint8_t portHigh;
    uint8_t portLow;
    uint16_t port;
  } TcpComms;

 /* Variables ----------------------------------------------------------------*/
#ifdef GLOBALS_OWNER
  TcpComms tcpComms;
  uint8_t connStatus     = 0;                 // WiFi connection Status flag
  uint8_t du800State     = STATE_NORMAL;      // DU800 state          
  uint8_t configuring    = 0;                 // WIFI configuration flag - Indicating we are busy changing the WiFi configuration         

  unsigned char TCPRXBuffer[TCP_RX_BUF_LEN];  // TCP data recieve buffer
  unsigned char TCPTXBuffer[TCP_TX_BUF_LEN];  // TCP data transmit buffer
  uint16_t TCPRXLength;                       // TCP received data length
  uint16_t TCPTXLength;                       // TCP transmit data length
  uint8_t  TCPReadIndex = 0;                  // TCP recieve buffer index pointer

  uint8_t  uartRXLength;                      // UART Data recieved length  
  uint8_t  uartTXLength;                      // UART Data to transmit length
  uint8_t  uartRXBuffer[UART_RX_BUF_LEN];     // UART recieve buffer
  uint8_t  uartTXBuffer[UART_TX_BUF_LEN];     // UART transmit buffer

  struct tm  t;
  time_t t_of_day;

  uint8_t rescanEnabled = 0;                  // Pause station rescanning
  uint8_t pauseRescan   = 0;
  uint16_t rescanTimer = 0;
#else
  extern TcpComms tcpComms;

  extern uint8_t connStatus;                        // WiFi connection Status flag
  extern uint8_t du800State;                        // DU800 state                
  extern uint8_t configuring;                       // WIFI configuration flag - Indicating we are busy changing the WiFi configuration         

  extern unsigned char TCPRXBuffer[TCP_RX_BUF_LEN]; // TCP data receive buffer
  extern unsigned char TCPTXBuffer[TCP_TX_BUF_LEN]; // TCP data transmit buffer
  extern uint16_t TCPRXLength;                      // TCP received data length
  extern uint16_t TCPTXLength;                      // TCP transmit data length
  extern uint8_t  TCPReadIndex;                     // TCP recieve buffer index pointer

  extern uint8_t  uartRXLength;                     // UART Data recieved length  
  extern uint8_t  uartTXLength;                     // UART Data to transmit length
  extern uint8_t  uartRXBuffer[UART_RX_BUF_LEN];    // UART recieve buffer
  extern uint8_t  uartTXBuffer[UART_TX_BUF_LEN];    // UART transmit buffer

  extern struct tm  t;
  extern time_t t_of_day;

  extern uint8_t rescanEnabled;
  extern uint16_t rescanTimer;
  extern int8_t trackingVariable;
  extern uint8_t pauseRescan;
#endif


 #ifndef NULL
    #define NULL 0
 #endif

/** @defgroup Exported_Constants
  *
  */ 


/** @defgroup Exported_Macros
  * 
  */ 


/** @defgroup Exported_Functions
  * 
  */ 

/** @defgroup Exported_Types
  * 
  */
typedef enum {BACKWARD = 0, FORWARD = !BACKWARD}  tBufDirection;
typedef enum {L_ENDIAN = 1, B_ENDIAN = !L_ENDIAN} tEndianess;
typedef enum {CRC_PASS = 0, CRC_FAIL = !CRC_PASS} tCRC;

#ifdef __cplusplus
}
#endif
  
#endif    /*_GLOBALS_H*/

/******END OF FILE****/
