/**
  ******************************************************************************
  * file    wifi_ap_config.c
  * author  David Groenewald
  * version V1.0.0
  * date    08/01/2018
  * brief   Configure ESP as an AP and setup DHCP server + TCP for data transmission
  ******************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _COMMANDTIMERS_H
#define _COMMANDTIMERS_H

#ifdef __cplusplus
 extern "C" {
#endif 

/* Defines -------------------------------------------------------------------*/
#define TCPIP_TIMEOUT   1000  // 1sec Timeout for TCP Packets
#define PD_CMD_TIMEOUT	100   // 100ms Timeout for PDx8x to respond to request

/* Includes ------------------------------------------------------------------*/
#include "commandTimers.h"
#include "esp_common.h"
#include "espconn.h"

/* Exported_Timer_Variables --------------------------------------------------*/
extern os_timer_t tcpipPacket_Timeout; // Timer to set timeout for TCPIP packets
extern os_timer_t pdCommand_Timeout;   // Timer to set timeout for PDx8x response

/* Exported_Functions --------------------------------------------------------*/
void tcpipPacketTimeout(void);  // Funtion called on TCPIP packet timeout
void commandTimeout(void);      // Funtion called on PDx8x response timeout


#ifdef __cplusplus
}
#endif
  
#endif    /*_COMMANDTIMERS_H*/

/******END OF FILE****/
