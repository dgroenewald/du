/**
  ******************************************************************************
  * file    spi_flash.h
  * author  David Groenewald
  * version
  * date    02/03/2016
  * brief   This file contains definitions for the DataConcentrator Flash commands
  ******************************************************************************
  */

#ifndef __SPIFLASHAPIS_H
#define __SPIFLASHAPIS_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "Esp_system.h"
#include "gpio.h"
#include "gpioConfig.h"

/* Exported types ------------------------------------------------------------*/
typedef enum
{
  QUICK     = 0,
  LONG      = 1
}tSpeed;

typedef struct {
   uint8_t  byte;
   uint8_t  resultByte;
   uint8_t  halfWord[2];
   uint16_t resultHalfWord;
} spi_TXRX_Data;


/* Exported types ------------------------------------------------------------*/
extern spi_TXRX_Data spi_rx_data;
extern spi_TXRX_Data spi_tx_data;

#define FAIL	0
#define PASS	1

/* Exported constants --------------------------------------------------------*/
#define DUMMY_BYTE    0xA5

/* NOTE: All these values are stored in the FIRST flash sector in the 8Mb external
           flash chip.
     The following are stored in this sector:
     Page 0 (0x0000 - 0x0FFFF)
       - Ethernet params
       - Type of firmware
       - Versions of firmware, bootloader  and apps
       - info about an app loaded above (whether the bootloader must clear the current app etc etc)
 */
 #define SPI_FLASH_ADDRESS_SIZE		1
 #define SPI_FLASH_START_ADDRESS	0x00000000

 /* Sectors on external 8Mb flash where parms
  * such as: - Ethernet parms
  *          - Timeouts
  *          - Configuration
  *          - Bootloader Apps
  *  are stored
  */
 #define SPI_FLASH_SECTOR0			0x00000000	// AP MODE - Ethernet parameters
 #define SPI_FLASH_SECTOR1			0x00010000	// STATION MODE - Transparent Command timeout
 #define SPI_FLASH_SECTOR2			0x00020000	// Configuration settings
 #define SPI_FLASH_SECTOR3			0x00030000	// Database status

 #define SPI_FLASH_SECTOR4			0x00040000	// Unused
 #define SPI_FLASH_SECTOR5			0x00050000	// Unused
 #define SPI_FLASH_SECTOR6			0x00060000	// Unused
 #define SPI_FLASH_SECTOR7			0x00070000	// Unused

 #define SPI_FLASH_SECTOR8			0x00080000	// Unused
 #define SPI_FLASH_SECTOR9			0x00090000	// Unused

 #define SPI_FLASH_SECTOR10			0x000A0000	// Unused
 #define SPI_FLASH_SECTOR11			0x000B0000	// Unused
 #define SPI_FLASH_SECTOR12			0x000C0000	// Unused
 #define SPI_FLASH_SECTOR13			0x000D0000	// Unused

 #define SPI_FLASH_SECTOR14			0x000E0000	// Unused
 #define SPI_FLASH_SECTOR15			0x000F0000	// Unused

/* Sector 0 */
 #define AP_MODE_ETH_PARMS    (SPI_FLASH_SECTOR0)
 #define AP_IP1_ADDRESS       (SPI_FLASH_SECTOR0 + SPI_FLASH_ADDRESS_SIZE*0)
 #define AP_IP2_ADDRESS			  (SPI_FLASH_SECTOR0 + SPI_FLASH_ADDRESS_SIZE*1)
 #define AP_IP3_ADDRESS			  (SPI_FLASH_SECTOR0 + SPI_FLASH_ADDRESS_SIZE*2)
 #define AP_IP4_ADDRESS			  (SPI_FLASH_SECTOR0 + SPI_FLASH_ADDRESS_SIZE*3)
 #define AP_SUBNET1_ADDRESS   (SPI_FLASH_SECTOR0 + SPI_FLASH_ADDRESS_SIZE*4)
 #define AP_SUBNET2_ADDRESS	  (SPI_FLASH_SECTOR0 + SPI_FLASH_ADDRESS_SIZE*5)
 #define AP_SUBNET3_ADDRESS	  (SPI_FLASH_SECTOR0 + SPI_FLASH_ADDRESS_SIZE*6)
 #define AP_SUBNET4_ADDRESS	  (SPI_FLASH_SECTOR0 + SPI_FLASH_ADDRESS_SIZE*7)
 #define AP_GATEWAY1_ADDRESS  (SPI_FLASH_SECTOR0 + SPI_FLASH_ADDRESS_SIZE*8)
 #define AP_GATEWAY2_ADDRESS  (SPI_FLASH_SECTOR0 + SPI_FLASH_ADDRESS_SIZE*9)
 #define AP_GATEWAY3_ADDRESS  (SPI_FLASH_SECTOR0 + SPI_FLASH_ADDRESS_SIZE*10)
 #define AP_GATEWAY4_ADDRESS  (SPI_FLASH_SECTOR0 + SPI_FLASH_ADDRESS_SIZE*11)
 #define AP_PORT_HIGH_ADDRESS (SPI_FLASH_SECTOR0 + SPI_FLASH_ADDRESS_SIZE*12)
 #define AP_PORT_LOW_ADDRESS	(SPI_FLASH_SECTOR0 + SPI_FLASH_ADDRESS_SIZE*13)

/* Sector 1 */
 #define STATION1_MODE_ETH_PARMS     (SPI_FLASH_SECTOR1)
 #define STATION1_IP1_ADDRESS        (SPI_FLASH_SECTOR1 + SPI_FLASH_ADDRESS_SIZE*0)
 #define STATION1_IP2_ADDRESS			   (SPI_FLASH_SECTOR1 + SPI_FLASH_ADDRESS_SIZE*1)
 #define STATION1_IP3_ADDRESS			   (SPI_FLASH_SECTOR1 + SPI_FLASH_ADDRESS_SIZE*2)
 #define STATION1_IP4_ADDRESS			   (SPI_FLASH_SECTOR1 + SPI_FLASH_ADDRESS_SIZE*3)
 #define STATION1_SUBNET1_ADDRESS    (SPI_FLASH_SECTOR1 + SPI_FLASH_ADDRESS_SIZE*4)
 #define STATION1_SUBNET2_ADDRESS	   (SPI_FLASH_SECTOR1 + SPI_FLASH_ADDRESS_SIZE*5)
 #define STATION1_SUBNET3_ADDRESS	   (SPI_FLASH_SECTOR1 + SPI_FLASH_ADDRESS_SIZE*6)
 #define STATION1_SUBNET4_ADDRESS	   (SPI_FLASH_SECTOR1 + SPI_FLASH_ADDRESS_SIZE*7)
 #define STATION1_GATEWAY1_ADDRESS   (SPI_FLASH_SECTOR1 + SPI_FLASH_ADDRESS_SIZE*8)
 #define STATION1_GATEWAY2_ADDRESS   (SPI_FLASH_SECTOR1 + SPI_FLASH_ADDRESS_SIZE*9)
 #define STATION1_GATEWAY3_ADDRESS   (SPI_FLASH_SECTOR1 + SPI_FLASH_ADDRESS_SIZE*10)
 #define STATION1_GATEWAY4_ADDRESS   (SPI_FLASH_SECTOR1 + SPI_FLASH_ADDRESS_SIZE*11)
 #define STATION1_PORT_HIGH_ADDRESS  (SPI_FLASH_SECTOR1 + SPI_FLASH_ADDRESS_SIZE*12)
 #define STATION1_PORT_LOW_ADDRESS	 (SPI_FLASH_SECTOR1 + SPI_FLASH_ADDRESS_SIZE*13)

/* Sector 2 */
 #define STATION2_MODE_ETH_PARMS     (SPI_FLASH_SECTOR2)
 #define STATION2_IP1_ADDRESS        (SPI_FLASH_SECTOR2 + SPI_FLASH_ADDRESS_SIZE*0)
 #define STATION2_IP2_ADDRESS			   (SPI_FLASH_SECTOR2 + SPI_FLASH_ADDRESS_SIZE*1)
 #define STATION2_IP3_ADDRESS			   (SPI_FLASH_SECTOR2 + SPI_FLASH_ADDRESS_SIZE*2)
 #define STATION2_IP4_ADDRESS			   (SPI_FLASH_SECTOR2 + SPI_FLASH_ADDRESS_SIZE*3)
 #define STATION2_SUBNET1_ADDRESS    (SPI_FLASH_SECTOR2 + SPI_FLASH_ADDRESS_SIZE*4)
 #define STATION2_SUBNET2_ADDRESS	   (SPI_FLASH_SECTOR2 + SPI_FLASH_ADDRESS_SIZE*5)
 #define STATION2_SUBNET3_ADDRESS	   (SPI_FLASH_SECTOR2 + SPI_FLASH_ADDRESS_SIZE*6)
 #define STATION2_SUBNET4_ADDRESS	   (SPI_FLASH_SECTOR2 + SPI_FLASH_ADDRESS_SIZE*7)
 #define STATION2_GATEWAY1_ADDRESS   (SPI_FLASH_SECTOR2 + SPI_FLASH_ADDRESS_SIZE*8)
 #define STATION2_GATEWAY2_ADDRESS   (SPI_FLASH_SECTOR2 + SPI_FLASH_ADDRESS_SIZE*9)
 #define STATION2_GATEWAY3_ADDRESS   (SPI_FLASH_SECTOR2 + SPI_FLASH_ADDRESS_SIZE*10)
 #define STATION2_GATEWAY4_ADDRESS   (SPI_FLASH_SECTOR2 + SPI_FLASH_ADDRESS_SIZE*11)
 #define STATION2_PORT_HIGH_ADDRESS  (SPI_FLASH_SECTOR2 + SPI_FLASH_ADDRESS_SIZE*12)
 #define STATION2_PORT_LOW_ADDRESS	 (SPI_FLASH_SECTOR2 + SPI_FLASH_ADDRESS_SIZE*13)

/* Sector 3 */
 #define WIFI_IP_MODE_SETTINGS   (SPI_FLASH_SECTOR3)
 #define WIFI_MODE_ADDRESS       (SPI_FLASH_SECTOR3 + SPI_FLASH_ADDRESS_SIZE*0)  // Mode (AP or Station) to configure the DU at startup
 #define IP_ADDRESS_MODE_ADDRESS (SPI_FLASH_SECTOR3 + SPI_FLASH_ADDRESS_SIZE*1)  // IP Mode (STATIC of DHCP) to configure the DU at startup)

/* Sector 4 */
 #define AP_SSID_PWD_ADDRESS   (SPI_FLASH_SECTOR4)
 #define AP_SSID_ADDRESS       (SPI_FLASH_SECTOR4 + SPI_FLASH_ADDRESS_SIZE*0)	// Are there ethernet parms stored in flash
 #define AP_PASSWD_ADDRESS     (AP_SSID_ADDRESS   + MAX_SSID_LENGTH)	// Are there ethernet parms stored in flash

/* Sector 5 */
// #define STATION_SSID_PWD_ADDRESS (SPI_FLASH_SECTOR5)

 #define STATION_SSID1_ADDRESS    (SPI_FLASH_SECTOR5)	// Are there ethernet parms stored in flash
 #define STATION_PWD1_ADDRESS	    (SPI_FLASH_SECTOR6)       //(SPI_FLASH_SECTOR4 + SPI_FLASH_ADDRESS_SIZE*32)	// Are there ethernet parms stored in flash
 #define STATION_SSID2_ADDRESS    (SPI_FLASH_SECTOR7)  //(SPI_FLASH_SECTOR4 + SPI_FLASH_ADDRESS_SIZE*97)	// Are there ethernet parms stored in flash
 #define STATION_PWD2_ADDRESS	    (SPI_FLASH_SECTOR8)

/* #define STATION_SSID1_ADDRESS    (SPI_FLASH_SECTOR5 + SPI_FLASH_ADDRESS_SIZE*0)	// Are there ethernet parms stored in flash
 #define STATION_PWD1_ADDRESS	    (STATION_SSID1_ADDRESS + MAX_SSID_LENGTH)       //(SPI_FLASH_SECTOR4 + SPI_FLASH_ADDRESS_SIZE*32)	// Are there ethernet parms stored in flash
 #define STATION_SSID2_ADDRESS    (STATION_PWD1_ADDRESS  + MAX_PASSWD_LENGTH + 1)  //(SPI_FLASH_SECTOR4 + SPI_FLASH_ADDRESS_SIZE*97)	// Are there ethernet parms stored in flash
 #define STATION_PWD2_ADDRESS	    (STATION_SSID2_ADDRESS + MAX_SSID_LENGTH   + 1)
 #define STATION_SSID3_ADDRESS    (STATION_PWD2_ADDRESS  + MAX_PASSWD_LENGTH + 1)
 #define STATION_PWD3_ADDRESS	    (STATION_SSID3_ADDRESS + MAX_SSID_LENGTH   + 1)
 #define STATION_SSID4_ADDRESS    (STATION_PWD3_ADDRESS  + MAX_PASSWD_LENGTH + 1)
 #define STATION_PWD4_ADDRESS	    (STATION_SSID4_ADDRESS + MAX_SSID_LENGTH   + 1)
*/

/* Sector 13 */ 
 #define FACTORY_STATION_SETTINGS   (SPI_FLASH_SECTOR13)
 #define FACTORY_SSID_ADDRESS       (FACTORY_STATION_SETTINGS + SPI_FLASH_ADDRESS_SIZE*0)	// Are there ethernet parms stored in flash
 #define FACTORY_PWD_ADDRESS	      (FACTORY_SSID_ADDRESS + MAX_SSID_LENGTH)       //(SPI_FLASH_SECTOR4 + SPI_FLASH_ADDRESS_SIZE*32)	// Are there ethernet parms stored in flash
 #define FACTORY_IP1_ADDRESS        (FACTORY_PWD_ADDRESS + 1)
 #define FACTORY_IP2_ADDRESS			  (FACTORY_IP1_ADDRESS + 1)
 #define FACTORY_IP3_ADDRESS			  (FACTORY_IP2_ADDRESS + 1)
 #define FACTORY_IP4_ADDRESS			  (FACTORY_IP3_ADDRESS + 1)
 #define FACTORY_SUBNET1_ADDRESS    (FACTORY_IP4_ADDRESS + 1)
 #define FACTORY_SUBNET2_ADDRESS	  (FACTORY_SUBNET1_ADDRESS + 1)
 #define FACTORY_SUBNET3_ADDRESS	  (FACTORY_SUBNET2_ADDRESS + 1)
 #define FACTORY_SUBNET4_ADDRESS	  (FACTORY_SUBNET3_ADDRESS + 1)
 #define FACTORY_GATEWAY1_ADDRESS   (FACTORY_SUBNET4_ADDRESS + 1)
 #define FACTORY_GATEWAY2_ADDRESS   (FACTORY_GATEWAY1_ADDRESS + 1)
 #define FACTORY_GATEWAY3_ADDRESS   (FACTORY_GATEWAY2_ADDRESS + 1)
 #define FACTORY_GATEWAY4_ADDRESS   (FACTORY_GATEWAY3_ADDRESS + 1)
 #define FACTORY_PORT_HIGH_ADDRESS  (FACTORY_GATEWAY4_ADDRESS + 1)
 #define FACTORY_PORT_LOW_ADDRESS	  (FACTORY_PORT_HIGH_ADDRESS + 1)

/* Sector 14 */
 #define PRIME_ADDRESS    (SPI_FLASH_SECTOR14)
 #define PRIME1_ADDRESS   (SPI_FLASH_SECTOR14 + SPI_FLASH_ADDRESS_SIZE*0)	// Is the external FLASH configured

/* Exported macro ------------------------------------------------------------*/
/* Select SPI FLASH: Chip Select pin low  */
#define SPI_FLASH_CS_LOW()       gpio16_output_set(0);	// Enable External SPI FLASH CS
/* Deselect SPI FLASH: Chip Select pin high */
#define SPI_FLASH_CS_HIGH()      gpio16_output_set(1);	// Disable External SPI FLASH CS

#define WRITE_PROTECT_HIGH()     GPIO_OUTPUT_SET(WR_PROT_IO_NUM, 0);	// Disable External SPI FLASH Write Protect
#define WRITE_PROTECT_LOW()      GPIO_OUTPUT_SET(WR_PROT_IO_NUM, 1);	// Enable External SPI FLASH Write Protect

/* Typedefs ------------------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/
/*----- High layer function -----*/
void SPI_Init(void);

void SPI_FLASH_Init(void);
void SPI_FLASH_SectorErase(uint32_t SectorAddr);
void SPI_FLASH_BulkErase(void);
void SPI_FLASH_PageWrite(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite);
void SPI_FLASH_BufferWrite(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite);
void SPI_FLASH_BufferRead(uint8_t* pBuffer, uint32_t ReadAddr, uint16_t NumByteToRead);
uint32_t SPI_FLASH_ReadID(void);
void SPI_FLASH_StartReadSequence(uint32_t ReadAddr);

void SPI_FLASH_Write(uint8_t *pBuffer, uint32_t WriteAddr, uint16_t NumBytesToWrite);
void SPI_FLASH_Read(uint8_t *pBuffer, uint32_t ReadAddr, uint16_t NumBytesToRead);
void update_flash(uint32_t Address);

/*----- Low layer function -----*/
uint8_t SPI_FLASH_ReadByte(void);
uint8_t SPI_FLASH_SendByte(uint8_t byte);
uint16_t SPI_FLASH_SendHalfWord(uint16_t HalfWord);
void SPI_FLASH_WriteEnable(void);
void SPI_FLASH_WaitForWriteEnd(void);
void FLASH_ReadHalfWord(uint32_t address, uint16_t * data);

#endif /* __SPIFLASHAPIS_H */
