/**
  ******************************************************************************
  * @file    gpio_config.h
  * @author  David Groenewald
  * @version V1.0.0
  * @date    18/12/2017
  * @brief   Header file for GPIO initialisation.
  ******************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __GPIOCONFIG_H__
#define __GPIOCONFIG_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "Esp_system.h"
#include "gpio.h"

/* Defines -------------------------------------------------------------------*/
/* Defines for the SPI and GPIO pins used to drive the SPI Flash */
// GPIO pin configuration
#define	 WR_PROT_IO_MUX  PERIPHS_IO_MUX_GPIO5_U	
#define  WR_PROT_IO_NUM	 5
#define  WR_PROT_IO_FUNC FUNC_GPIO5
#define  WR_PROT_IO_PIN  GPIO_Pin_5
/* Macros --------------------------------------------------------------------*/

/* Function Prototypes -------------------------------------------------------*/
void GPIO_Init(void);

#endif //__GPIOCONFIG_H_

/* END OF FILE */
