/**
  ******************************************************************************
  * @file    tasks.h
  * @author  David Groenewald
  * @version V1.0.0
  * @date    08/01/2018
  * @brief   Header file for RTOS tasks prototypes etc.
  ******************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TASKS_H__
#define __TASKS_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "esp_common.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"

/* Defines -------------------------------------------------------------------*/
// Task priorities
#define adcTaskPriority           1
#define wifiConfigTaskPriority    1
#define ledTaskPriority           1
#define uartTaskPriority          30
#define processTCPIPTaskPriority  30
#define sendTCPIPTaskPriority     1


/* Macros --------------------------------------------------------------------*/

/* RTOS Handles --------------------------------------------------------------*/
/* Semaphores */
xSemaphoreHandle xtcpSemaphoreHandle;         // Semaphore Handle to process TCP data
xSemaphoreHandle xtcpSendSemaphoreHandle;     // Semaphore Handle to send TCPIP Data

/* Task Handles */
xTaskHandle xWiFiConfigTaskHandle;    // Wifi Config Task Handle
xTaskHandle xADCTaskHandle;           // ADC Task handle 
xTaskHandle xLEDTaskHandle;           // LED control task handle 
xTaskHandle xUartTaskHandle;          // UART Task and Queue handles

/* Queues */
xQueueHandle xQueueUart;      // Queue for UART recieved data


/* Variables -----------------------------------------------------------------*/

/* Function Prototypes -------------------------------------------------------*/
// RTOS Task functions
void adcReadTask(void* pvParameters);
void adcInitTask(void);

void wifiConfigTask(void* pvParameters);
void wifiConfigInitTask(void);

void ledBlinkTask(void* pvParameters);
void ledInitTask(void); 

void processTCPTask(void* pvParameters);
void processTCPInitTask(void); 

void sendTCPTask(void* pvParameters);
void sendTCPInitTask(void);

#endif //_TASKS_H_

/* END OF FILE */
