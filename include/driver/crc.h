/**
  ******************************************************************************
  * @file    crc.h
  * @author  Mark Ferris
  * @version V1.0.0
  * @date    3/03/2010
  * @brief   Header file for crc.c module.
  ******************************************************************************
  */ 
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _CRC_H
#define _CRC_H

#ifdef __cplusplus
 extern "C" {
#endif 

/* Includes ------------------------------------------------------------------*/
#include "esp_common.h"

/** @defgroup Exported_Constants
  *
  */ 


/** @defgroup Exported_Macros
  * 
  */ 


/** @defgroup Exported_Functions
  * 
  */ 
uint8_t  crc_crcTableEntry 		(uint8_t index);
char  crc_calculateCRC (char *buffer, uint16_t noBytes, uint8_t dir, uint8_t id);

#ifdef __cplusplus
}
#endif
  
#endif    /*_CRC_H*/

/******END OF FILE****/
