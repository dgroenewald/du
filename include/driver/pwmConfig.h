/**
  ******************************************************************************
  * @file    pwm_config.h
  * @author  David Groenewald
  * @version V1.0.0
  * @date    18/12/2017
  * @brief   Header file for PWM pin configuration.
  ******************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __PWMCONFIG_H__
#define __PWMCONFIG_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "esp_common.h"
#include "gpio.h"
#include "pwm.h"

/* Defines -------------------------------------------------------------------*/
// PWM pin configuration
// Red LED
#define PWM_1_OUT_IO_MUX	PERIPHS_IO_MUX_MTDO_U
#define PWM_1_OUT_IO_NUM	15
#define PWM_1_OUT_IO_FUNC	FUNC_GPIO15

// Green LED
#define PWM_2_OUT_IO_MUX	PERIPHS_IO_MUX_GPIO4_U
#define PWM_2_OUT_IO_NUM	4
#define PWM_2_OUT_IO_FUNC	FUNC_GPIO4

#define PWM_NUM_CHANNEL_NUM 2  //number of PWM Channels

#define RLED	0	// Red LED is on PWM channel 0
#define GLED	1	// Green LED is on PWM channel 1

/* Variables -----------------------------------------------------------------*/
extern uint16_t dutyGreen, dutyRed;

/* Macros --------------------------------------------------------------------*/

/* Function Prototypes -------------------------------------------------------*/
void PWM_Init(void);
void setPWM();//int dutyCycle, uint8 channel);

#endif //_PWMCONFIG_H_

/* END OF FILE */
