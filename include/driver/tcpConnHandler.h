/**
  ******************************************************************************
  * @file    tasks.h
  * @author  David Groenewald
  * @version V1.0.0
  * @date    08/01/2018
  * @brief   Header file for RTOS tasks prototypes etc.
  ******************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TCPCONNHANDLER_H__
#define __TCPCONNHANDLER_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/

/* Defines -------------------------------------------------------------------*/

/* Macros --------------------------------------------------------------------*/

/* Variables -----------------------------------------------------------------*/
extern uint16_t ReadIndex;
extern uint16_t msgLength;
extern uint8_t  discardPacket;
/* Function Prototypes -------------------------------------------------------*/
void ICACHE_FLASH_ATTR tcp_connect_cb(void *arg);
void ICACHE_FLASH_ATTR tcp_disconnect_cb(void *arg);
void ICACHE_FLASH_ATTR recv_cb(void *arg, char *data, uint16_t len);

#endif //_TCPCONNHANDLER_H_

/* END OF FILE */
