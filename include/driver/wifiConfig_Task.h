/**
  ******************************************************************************
  * @file    connectionConfig.h
  * @author  David Groenewald
  * @version V1.0.0
  * @date    05/09/2018
  * @brief   Header file for AP/Station mode configuration.
  ******************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __WIFCONFIG_H__
#define __WIFICONFIG_H__

#ifdef __cplusplus
extern "C" {
#endif

#define STATION_SSID1     "Nortech_VCC_WiFi"
#define STATION_PASSWD1   "midlands2012"

#define STATION_SSID2     " "
#define STATION_PASSWD2  	" "

#define AP_SSID			 	    "DU800"
#define AP_PASSWD 			  "895SW8000"

/* Macros --------------------------------------------------------------------*/

/* Includes ------------------------------------------------------------------*/

/* Defines -------------------------------------------------------------------*/
/* Static IP ADDRESS: IP_ADDR0.IP_ADDR1.IP_ADDR2.IP_ADDR3 */
#define DEFAULT_IP_ADDR0   10
#define DEFAULT_IP_ADDR1   66
#define DEFAULT_IP_ADDR2   7
#define DEFAULT_IP_ADDR3   158

/* Gateway Address */
#define DEFAULT_GW_ADDR0   10
#define DEFAULT_GW_ADDR1   66
#define DEFAULT_GW_ADDR2   7
#define DEFAULT_GW_ADDR3   1

/* NETMASK */
#define DEFAULT_NETMASK_ADDR0   255
#define DEFAULT_NETMASK_ADDR1   255
#define DEFAULT_NETMASK_ADDR2   255
#define DEFAULT_NETMASK_ADDR3   0

/* Port */
#define DEFAULT_PORT	  5001

/* WiFi connection Mode - AP or Station Mode */ 
#define MODE_AP           0x02
#define MODE_STATION      0x03
/* DHCP or Static IP ----------------------------------------------------------*/
#define DHCP			        0x04
#define STATIC_IP		      0x05

#define MAX_SSID_LENGTH    32
#define MAX_PASSWD_LENGTH  64
#define MAX_NUM_OF_SSIDS   4

/* Variables -----------------------------------------------------------------*/
extern uint8_t wifiMode;
extern uint8_t ipAddressMode;
extern uint8_t flashPrimed;
extern uint32_t last_addr;

extern uint8_t ssidLength;
extern uint8_t passwdLength;
extern char ssidBuffer[MAX_NUM_OF_SSIDS][MAX_SSID_LENGTH];
extern char passwdBuffer[MAX_NUM_OF_SSIDS][MAX_PASSWD_LENGTH];

/* Function Prototypes -------------------------------------------------------*/
void ICACHE_FLASH_ATTR wifiModeConfig(void);
void ICACHE_FLASH_ATTR wifiInitTask();
void ICACHE_FLASH_ATTR wifi_event_cb(System_Event_t *event);

#ifdef __cplusplus
}
#endif

#endif //_WIFICONFIG_H_

/* END OF FILE */
